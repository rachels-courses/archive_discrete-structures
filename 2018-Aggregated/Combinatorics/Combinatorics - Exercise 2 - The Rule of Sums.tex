\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Combinatorics: The Rule of Sums}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 5.2}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 6.2, 6.3}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Review: Structures}

    \begin{intro}{Structures}
        \begin{center}
            \begin{tabular}{l | c | c | c }
                \textbf{}   & \textbf{Repeats}      & \textbf{Order} & \textbf{Formula}
                \\          & \textbf{allowed?}     & \textbf{matters?}     &

                \\ \hline
                \textbf{Permutation} &  &  &
                \\ $n$ items to choose from & no & yes & $P(n,r) = \frac{n!}{(n-r)!}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Set} &  &  &
                \\ $n$ items to choose from & no & no & $C(n,r) = \frac{n!}{r!(n-r)!}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Ordered list} &  &  &
                \\ $n$ items to choose from & yes &  yes & $n^{r}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Unordered list} &  &  &
                \\ $n$ different types & yes & no & $C(n+r-1, n)$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Unordered list}  &  &  &
                \\ \textbf{for Binary Strings} &  &  &
                \\ $r$ 1's & yes & no & $C(n, r)$
                \\ $n-r$ 0's & &
            \end{tabular}
        \end{center}
    \end{intro}

    \newpage
    
    \subsection*{The Rule of Sums}

    \begin{intro}{The Rule of Sums}
        In combinatorics, the rule of sum or addition principle is a basic counting principle.
        Stated simply,
        \textbf{it is the idea that if we have A ways of doing something and B ways of doing
        another thing and we can not do both at the same time, then there are A + B ways to choose one of the actions.}
        \footnote{From https://en.wikipedia.org/wiki/Rule\_of\_sum}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        An arcade machine prompts you to enter your initials for the high-score table.
        You can enter any capital letter (A-Z means 26 options), or any number (0-9 means 10 options).
        You have 3 slots to enter letters. How many ways can you enter 3 characters, if
        you do all letters \textbf{or} all numbers?

        \begin{itemize}
            \item   Scenario 1:
            \begin{itemize}
                \item   How many options to choose from ($n$):  \vspace{1cm}
                \item   How many characters to choose ($r$):    \vspace{1cm}
                \item   Result 1 ($n^{r}$):                     \vspace{1cm}
            \end{itemize}
            
            \item   Scenario 2:
            \begin{itemize}
                \item   How many options to choose from ($n$):  \vspace{1cm}
                \item   How many characters to choose ($r$):    \vspace{1cm}
                \item   Result 2 ($n^{r}$):                     \vspace{1cm}
            \end{itemize}

            \item   Result: (Result 1 + Result 2): 
        \end{itemize}
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        In a class of 12 students, there are 5 CS students, 3 IT students, and 4 Math students.
        You are going to elect a President, Vice President, and Secretary.
        How many ways can you elect three students, if each role is filled by students with the same major?
        
        \begin{itemize}
            \item   Does order matter?          \inlinehint{Is this a set \{Rai, Rose, Rahaf\}, or would it be an ordered group (Rai, Rose, Rahaf) ?}
                    \vspace{0.5cm}
            \item   Are repetitions allowed?    \inlinehint{Can somebody be chosen more than once?}
                    \vspace{0.5cm}
            \item   What structure is this?
                    \vspace{1cm}
            \item   Scenario 1: Each role is filled only by CS students.        \vspace{2cm}
            \item   Scenario 2: Each role is filled only by IT students.        \vspace{2cm}
            \item   Scenario 3: Each role is filled only by Math students.      \vspace{2cm}
            \item   Result: (Scenario 1 OR Scenario 2 OR Scenario 3)            \vspace{2cm}
        \end{itemize}
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
            \small You are drawing a card from a deck of 52 cards. How many ways are there to... \normalsize

            \deckofcards

            \begin{enumerate}
                \item[a.]   Get one red card?             \vspace{2cm}

                \item[b.]   Get one Jack OR a Queen?      \vspace{2cm}

                \item[c.]   Get one club OR a diamond?    \vspace{2cm}

                \item[d.]   Get one face card OR an Ace?  \vspace{2cm}

                \item[e.]   Get two Jacks OR two Queens?  \vspace{2cm}
            \end{enumerate}
    \end{questionNOGRADE}

    \newpage

    \begin{intro}{The rule of sums with overlap}
        If the list to count can be split into two pieces of size $x$
        and $y$, and the pieces have $z$ objects in common, then the original
        list has $x + y - z$ entries. In terms of sets, we can write this as
        $n(A \cup B) = n(A) + n(B) - n(A \cap B)$ for all sets $A$ and $B$.
    \footnote{From Discrete Math Mathematical Reasoning and Proofs with Puzzles, Patterns and Games, by Ensley and Crawley}
    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
            \small You are drawing a card from a deck of 52 cards. How many ways are there to... \normalsize

            \deckofcards

            \begin{enumerate}
                \item   Get a Diamond card?                 \vspace{2cm}

                \item   Get a an Ace card?                  \vspace{2cm}

                \item   Get an Ace card OR a Diamond card?
                        \inlinehint{Use the Rule of Sums with Overlap}
                        \vspace{2cm}
            \end{enumerate}
    \end{questionNOGRADE}
    
    \newpage
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        At a muffin shop, you are going to buy two muffins - one for you, and one for your friend.
        There are chocolate muffins, banana nut muffins, and pumpkin muffins available today.
        How many ways are there to buy two muffins where you and your friend both get the same kind?
        \vspace{6cm}
    \end{questionNOGRADE}
    
    %\stepcounter{question}
    %\begin{questionNOGRADE}{\thequestion}
        %You're going with a group of 20 to the zoo. There are 5 parents and 15 kids.
        %Your car can hold 4 (other) people. How many ways can you take a group of
        %4 people with you, if there is \underline{at least} two parents in your car?
        %\\ \inlinehint{Scenarios: 2 parents, 3 parents, 4 parents}
        %\vspace{3cm}
    %\end{questionNOGRADE}


\input{../BASE-4-FOOT}
