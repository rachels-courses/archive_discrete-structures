\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Combinatorics: Intro and Structures}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 5.1, 5.2, 5.3, 5.4}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 6.1, 6.2, 6.3}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}


\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

    \section*{\laTitle}
    \footnotesize
    Combinatorics are counting problems, asking how many different combinations
    or outcomes there can be given some input(s), and when selecting a certain
    amount of items. These problems differ in how you solve them based on
    the type of structure.

    \begin{intro}{Types of structures}
        These are the four types of structures we will be working with
        in this section.

        \begin{center}
            \begin{tabular}{l | c | c | c }
                \textbf{}   & \textbf{Repeats}      & \textbf{Order} & \textbf{Formula}
                \\          & \textbf{allowed?}     & \textbf{matters?}     &

                \\ \hline
                \textbf{Permutation} &  &  &
                \\ $n$ items to choose from & no & yes & $P(n,r) = \frac{n!}{(n-r)!}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Set} &  &  &
                \\ $n$ items to choose from & no & no & $C(n,r) = \frac{n!}{r!(n-r)!}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Ordered list} &  &  &
                \\ $n$ items to choose from & yes &  yes & $n^{r}$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Unordered list} &  &  &
                \\ $n$ different types & yes & no & $C(n+r-1, r)$
                \\ Select $r$ items & &

                \\ \hline
                \textbf{Unordered list}  &  &  &
                \\ \textbf{for Binary Strings} &  &  &
                \\ $r$ 1's & yes & no & $C(n, r)$
                \\ $n-r$ 0's & &
            \end{tabular}
        \end{center}

        The challenging part of combinatorics is \textbf{figuring out which structure}
        is described in a problem, and what values to plug in. In this part
        we will get acquainted with simple problems for each structure,
        and in future exercises we will look at rules needed for more sophisticated problems.
    \end{intro} \normalsize

    \newpage

    \subsection*{Ordered Lists}

        \begin{center}
            \textbf{Order matters} \tab \textbf{Repeats allowed}

            Given $n$ options, select $r$ items: There are \large $n^{r}$ \normalsize ways to choose.
        \end{center}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        You are rolling two dice: a red die and a green die. How many results are possible?

        \begin{itemize}
            \item   There are $n$ total outcomes per one die:   \vspace{1cm}
            \item   We are rolling $r$ dice:                    \vspace{1cm}
            \item   Result, $n^{r}$:                            \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        An arcade machine prompts you to enter your initials for the high-score table.
        You can enter any capital letter (A-Z means 26 options), or any number (0-9 means 10 options).
        You have 3 slots to enter letters. How many ways can you write in a name?

        \begin{itemize}
            \item   There are $n$ total options to choose from, for one ``space'':  \vspace{1cm}
            \item   We have $r$ spaces to fill:                                     \vspace{1cm}
            \item   Result, $n^{r}$:                                                \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \newpage
    \subsection*{Permutations}

        \begin{center}
            \textbf{Order matters} \tab \textbf{Repeats NOT allowed}

            Given $n$ options, select $r$ items: $$P(n,r) = \frac{n!}{(n-r)!}$$ ways to choose.
        \end{center}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        In a class of 10 students, you are going to elect a President, Vice President, and Secretary.
        How many ways can you elect three students?

        \begin{itemize}
            \item   There are $n$ total students to choose from:    \vspace{0.5cm}
            \item   We are selecting for $r$ different roles:       \vspace{0.5cm}
            \item   Result, $P(n,r)$:                               \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        A class of 10 students need to line up for lunch. How many ways
        are there for all students to line up?

        \begin{itemize}
            \item   There are $n$ total students to line up from:    \vspace{0.5cm}
            \item   We are going to line up $r$ students: \inlinehint{(Hint: same as $n$)}      \vspace{0.5cm}
            \item   Result, $P(n,r)$:                               \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \newpage
    \subsection*{Sets}

        \begin{center}
            \textbf{Order DOESN'T matter} \tab \textbf{Repeats NOT allowed}

            Given $n$ options, select $r$ items: $$C(n,r) = \frac{n!}{r!(n-r)!}$$ ways to choose.
        \end{center}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        In a class of 10 students, you are going to choose 3 students to be on
        the holiday planning committee. How many ways can you elect three students for the committee?
        \inlinehint{Here, the students all fulfill the same role, so there is no difference between
        choosing the first student, second student, or third student.}

        \begin{itemize}
            \item   There are $n$ total students to choose from:    \vspace{0.5cm}
            \item   We are selecting $r$ students for the role:     \vspace{0.5cm}
            \item   Result, $C(n,r)$:                               \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        There is a box of marbles that has 6 green and 4 blue.
        How many ways are there to select 2 green marbles out of the box?

        \begin{itemize}
            \item   There are $n$ total green marbles:          \vspace{0.5cm}
            \item   We are going to pick out $r$ green marbles: \vspace{0.5cm}
            \item   Result, $C(n,r)$:                           \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \newpage
    \subsection*{Unordered Lists}

        \begin{center}
            \textbf{Order DOESN'T matter} \tab \textbf{Repeats allowed}

            Binary String: Given $r$ 1's and $n-r$ 0's, there are $C(n,r)$ ways to choose.
        \end{center}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        How many ways are there to build a binary string (1's and 0's only)
        of length 4 that has only one 1?
        
        \begin{itemize}
            \item   There are $r$ 1's:
                    \vspace{1cm}
            \item   There are $n-r$ 0's:
                    \vspace{1cm}
            \item   $n$ is:
                    \vspace{1cm}
            \item   Result, $C(n,r)$:
                    \vspace{1cm}
        \end{itemize}

    \end{questionNOGRADE}

    \newpage

        \begin{center}
            \textbf{Order DOESN'T matter} \tab \textbf{Repeats allowed}
            \small
            
            Selection way: Given $n$ types of items and $r$ items to choose, there are $C(n+r-1, r)$ ways to choose. ~\\~\\
            Binary String way: Given $r$ 1's and $n-r$ 0's, there are $C(n,r)$ ways to choose.
            \normalsize
        \end{center}
        
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}

        \begin{wrapfigure}{r}{0.3\textwidth}
            \begin{center}
                \includegraphics[height=3cm]{images/donutbox2.png}
            \end{center}
        \end{wrapfigure}

        \small At a donut shop, we are going to buy 6 donuts. The store sells 3 types of donuts. \\ \normalsize

        ~\\
        How many ways are there to buy 6 donuts?

        \begin{itemize}
            \item   Selection way:
                \begin{itemize}
                    \item   We are choosing $r$ donuts:
                            \vspace{0.5cm}
                    \item   There are $n$ options for each donut:
                            \vspace{0.5cm}
                    \item   Result, $C(n+r-1,r)$:
                            \vspace{1cm}
                \end{itemize}
            \item   Binary string way: We're going to consider 0's to represent the donuts. \\
                    We're going to consider 1's to be separators \textit{between each type} of donut,
                    meaning that if there are 3 types of donuts, there are 2 \textit{separators}.
                \begin{itemize}
                    \item   There are $r$ 1's:
                            \vspace{0.5cm}
                    \item   There are $n-r$ 0's:
                            \vspace{0.5cm}
                    \item   $n$ is:
                            \vspace{0.5cm}
                    \item   Result, $C(n,r)$:
                            \vspace{1cm}
                \end{itemize}
        \end{itemize}
    \end{questionNOGRADE}



\input{../BASE-4-FOOT}
