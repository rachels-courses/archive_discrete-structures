\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Combinatorics: Combinations}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 5.3}
%\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter ?}
\newcommand{\laTextbookB}   {}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}
    
    \subsection*{Combinations and Permutations}

    \begin{intro}{Permutations}
        A permutation is a type of structure that describes counting when
        \textbf{order matters} and \textbf{repetitions are not allowed}.

        ~\\  A permutation is written as $P(n, r)$ where $n$ is the amount of
        items we have to choose from, and $r$ is the amount of items we
        are selecting. The formula for this is:

        $$ P(n, r) = \frac{n!}{(n-r)!} $$

        ~\\ Where $n!$ is $n$-factorial. Also note that $0! = 1$.
    \end{intro}
    \begin{intro}{Combinations}
        In mathematics, a combination is selection of items from a collection,
        such that (unlike permutations) the order of selection does not matter.
        (...) The number of $r$-combinations from a given set of $n$ elements
        is often denoted in elementary combinatorics texts by $C(n,r)$.
        \footnote{From https://en.wikipedia.org/wiki/Combinations}

        For a combination of length $r$ from a set of $n$ elements:
        $$C(n,r) = \frac{n!}{(n-r)! \cdot r!}$$

        (Note that the book uses $r$ and Wikipedia uses $k$.)
    \end{intro}
    

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        How many ways can you rearrange the letters in the word ``DOG''?
        \begin{wrapfigure}{r}{0.2\textwidth}\centering
            \includegraphics[scale=0.9]{images/53letters.png}
        \end{wrapfigure}

        ~\\
        \begin{tabular}{l l l}
            Repetition allowed? & \Circle\ Yes & \Circle\ No \\
            Order matters? & \Circle\ Yes & \Circle\ No \\
            Structure? \\
            Solve:      \vspace{6cm}
        \end{tabular}
    \end{questionNOGRADE}
    
    \hrulefill
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \begin{wrapfigure}{r}{0.2\textwidth}\centering
            \includegraphics[scale=0.7]{images/53line.png}
        \end{wrapfigure}
        In how many ways can 10 children line up for lunch?
        
        ~\\
        \begin{tabular}{l l l}
            Repetition allowed? & \Circle\ Yes & \Circle\ No \\
            Order matters? & \Circle\ Yes & \Circle\ No \\
            Structure? \\
            Solve:      \vspace{4cm}
        \end{tabular}
    \end{questionNOGRADE}

    \newpage



    \begin{intro}{The Rule of Sums:} \small
        If we have A ways of doing something and B ways of doing
        another thing and we can not do both at the same time, then there are A + B ways to choose one of the actions.
        \footnote{From https://en.wikipedia.org/wiki/Rule\_of\_sum}

        \paragraph{The Rule of Complements:}
    
        If there are $x$ objects, and $y$ of those objects have a particular property,
        then the number of those objects that do \textbf{not} have that particular
        property is $x - y$.
        \footnote{From Discrete Mathematics, Ensley and Crawley, page 390}

        \paragraph{The Rule of Products:}
    
        If there are $a$ ways of doing something and $b$
        ways of doing another thing, then there are $a \cdot b$ ways of performing both actions.
        \footnote{From https://en.wikipedia.org/wiki/Rule\_of\_product}

    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        \begin{wrapfigure}{r}{0.4\textwidth}\centering
            \includegraphics[scale=1.0]{images/53it.png}
        \end{wrapfigure}
        In a class of 10 students, there are 3 IT majors and 7 CS majors.
        If 4 board positions had to be filled for the computer club,
        how many ways would there be to fill the positions with the given constraints?

        ~\\

        \begin{enumerate}
            \item[a.]   No constraints - any student can be on the board.   \vspace{1cm}
            \item[b.]   There must be exactly 2 IT students and 2 CS students on the board. \vspace{2cm}
            \item[c.]   There must be \textit{at least} 2 IT students on the board.
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Suppose we are going to receive a shipment of 50 games on floppy disk for our vintage game store.
        Each box of 50 generally has 3 defective floppies.
        For the shipment, we are going to select 5 games to feature in a display.

        \begin{enumerate}
            \item[a.]   How many total good floppies are there?                 \vspace{1cm}
            \item[b.]   How many total bad floppies are there?                  \vspace{1cm}
            \item[c.]   How many ways could we choose 5 games to feature?       \vspace{1cm}
            
            \item[d.]   How many ways contain \textit{no} defective floppies?   \vspace{1cm}
            
            \item[e.]   Using the Rule of Products, determine how many ways that contain all 3 defective floppies?
                        \vspace{2cm}
                        
            \item[f.]   Using the Rule of Sums and the Rule of Products, determine
                        how many ways contain \textit{at least one} defective floppy.   \vspace{2cm}

            \item[g.]   Using the Rule of Complements, determine
                        how many ways contain \textit{at least one} defective floppy.
                        The answer for (f) and (g) should match.    \vspace{2cm}
        \end{enumerate}
        
    \end{questionNOGRADE}

    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        There's a bargain bin that has 5 PC games, 3 Playstation games, and 8 Xbox games.

        \begin{enumerate}
            \item[a.]   How many total games are there? \vspace{2cm}
            
            \item[b.]   If you're grabbing 4 games to buy, you don't care about the order
                        that you pull the games out. How many ways can 4 games be selected?
                        \vspace{3cm}

            \item[c.]   How many ways can you select 4 games that are all for the same console?
                        \textit{(Hint: This means the Playstation games don't get counted.
                        Also, which Rule are you going to use to solve this?)}
                        \vspace{3cm}

            \item[d.]   How many selections of 4 games are there, where you have 2 for one platform,
                        and 2 for another platform?
                        \textit{(Hint: What Rules apply here?)}
                        \vspace{2cm}
        \end{enumerate}
    \end{questionNOGRADE}
    


\input{../BASE-4-FOOT}
