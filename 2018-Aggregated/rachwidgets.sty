% Widgets I use for my school stuff - exams, labs, programming assignments, etc.
% https://gitlab.com/RachelWilShaSingh/latex-stuff/

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{rachwidgets}

\usepackage[utf8]{inputenc} 
\usepackage{graphicx}           % \includegraphics
\usepackage{amsfonts}           % math symbols
\usepackage{listings}           % source code listings
\usepackage{framed}             % frames
\usepackage{mdframed}           % frames
\usepackage{color}              % colors
\usepackage{wrapfig}            % floating figures
\usepackage{subcaption}         % figure captions
\usepackage{wasysym}            % glyphs
\usepackage{multirow}           % multi-row in tables
\usepackage{dirtree}            % directory tree structure
\usepackage{fancyhdr}           % control of headers / footers
\usepackage{lastpage}           % for page #s
\usepackage{boxedminipage}      % boxed mini-pages
\usepackage{lipsum}             % placeholder text
\usepackage{tikz}               % draw stuff
\usepackage{circuitikz}         % draw circuits
\usepackage{fancyhdr}           % header/footer
\usepackage{tabularx}           % fancier tables
\usepackage{pifont}             % For icons like \xmark
\usepackage{centernot}

\usepackage{colortbl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Symbols %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\xmark}{\ding{55}\ }  % "X" mark, \checkmark is already available

% Radio button / circle
% From https://tex.stackexchange.com/questions/236041/how-to-typeset-a-radio-button
\newcommand{\radio}{\ooalign{\hidewidth$\bullet$\hidewidth\cr$\ocircle$}}
% Empty circle:             $\ocircle$
% Circle with small dot:    $\odot$
% Circle with full dot:     \radio

% Checkbox / box
% \Square

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Custom Colors %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Used for my example code output:
\definecolor{commentz}{rgb}{0,0.02,0.6}
\definecolor{keywordz}{rgb}{0.69,0.18, 0.36}
\definecolor{backgroundz}{rgb}{1.0, 0.95, 0.77}
\definecolor{textz}{rgb}{0.54, 0.27, 0.07}
\definecolor{hintboxz}{rgb}{0.9, 1.0, 0.9}
\definecolor{errorboxz}{rgb}{1.0, 0.9, 0.9}
\definecolor{previewbg}{rgb}{0.9, 0.9, 0.9}
\definecolor{previewfg}{rgb}{0, 0, 0}
\definecolor{textbg}{rgb}{1, 1, 1}
\definecolor{textfg}{rgb}{0, 0, 0}

% Framed boxes for introductory content / hints / common errors
\definecolor{intro}{rgb}{0.9, 0.9, 1.0}
\definecolor{error}{rgb}{1.0, 0.9, 0.9}
\definecolor{hint}{rgb}{0.9, 1.0, 0.9}

% Accessibility...
\definecolor{colorblind_dark}{rgb}{0.23, 0.28, 0.28}
\definecolor{colorblind_medium_blue}{rgb}{0.07, 0.40, 0.97}
\definecolor{colorblind_medium_orange}{rgb}{0.97, 0.35, 0.0}
\definecolor{colorblind_medium_gray}{rgb}{0.62, 0.69, 0.69}
\definecolor{colorblind_light_blue}{rgb}{0.63, 0.85, 1.0}
\definecolor{colorblind_light_orange}{rgb}{1.0, 0.65, 0.39}
\definecolor{colorblind_light_gray}{rgb}{0.76, 0.85, 0.89}

% Table cells for truth tables with colorblind-friendly colors
\newcommand\true[0]{ \cellcolor{colorblind_light_blue}\textbf T }
\newcommand\false[0]{ \cellcolor{colorblind_medium_orange}\color{white} \textbf F }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Text formatting %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Add an inline hint in the document
\newenvironment{inlinehint}[1] {\textcolor{gray}{ \footnotesize\textit{ #1 }}} { \normalsize }

% Add a tab in the document
% \tab  \tab[2cm]   \tab[2.5cm]
\newcommand\tab[1][1cm]{\hspace*{#1}}

% Fill in the blank region
\newcommand\fitb[1][1cm]{ $\rule{#1}{0.15mm}$ }

% Boxes for topic introductions / hints / common errors, to help
% break up the flow of the page.

% Intro box %
% \begin{intro}{The General Product Rule}
% \end{intro}
\newenvironment{intro}[1]{  \begin{mdframed}[backgroundcolor=intro]\noindent \textbf{ #1 } \\ } { \end{mdframed} }

% Hint box %
% \begin{hint}{Hint}
% \end{hint}
\newenvironment{hint}[1]{   \begin{mdframed}[backgroundcolor=hint]\noindent \textbf{ #1 } \\ } { \end{mdframed} }

% Error box %
% \begin{error}{Common error!}
% \end{error}
\newenvironment{error}[1]{  \begin{mdframed}[backgroundcolor=error]\noindent \textbf{ #1 } \\ } { \end{mdframed} }

% Quick citation %
\newcommand\quickcite[1] { \begin{flushright} \scriptsize #1 \normalsize \end{flushright} }
\newcommand\quickcitebook[4] { \begin{flushright} \scriptsize From #1, #2, #3, #4 \normalsize \end{flushright} }

% Other markings for questions %
\newcommand\markdifficult[0] { \color{orange} \scriptsize \textbf{(Students usually have trouble with this.)} \normalsize \color{black} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Exam Stuff %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Embed your answer key within the same file so you don't have to
% maintain as much stuff. Set to false to make the version to give
% to the students.
\newtoggle{answerkey}
\toggletrue{answerkey}

% Don't want this to show up on the answer key version of something?
% Embed it in \notonkey{ this won't show up }{}
\newcommand\notonkey[2] {\iftoggle{answerkey}{ \color{red} #2 \color{black} }{  #1 }}

% Mark solutions in red
% \begin{answer} the solution is ... \end{solution}
\newenvironment{answer}[0] { \color{red} \small } { \normalsize \color{black} }

% Make the solution only show up when the answer key is enabled.
% \solution{ Viewable on answerkey=true }{ Viewable when answerkey=false }
\newcommand\solution[2] {\iftoggle{answerkey}{\begin{answer}#1\end{answer}}{#2}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Questions %

% Weighted grading scale, I have questions where you can get 0% credit,
% 25%, 50%, 75%, or 100% credit. These are checkboxes marked 0, 1, 2, 3, and 4.
\newcommand\grader[0] { {\small \Square\ 0 \Square\ 1 \Square\ 2 \Square\ 3 \Square\ 4} }

% \newcounter{question}

% Arguments:    #1 = question number
%               #2 = question name
%               #3 = extra info (e.g., chapter)
%               #4 = weight of question
\newenvironment{examquestionpercent}[4] {\hrulefill ~\\ \textbf{(\footnotesize{#4\%}) Question #1: #2 #3 } \hfill \grader \\ ~ \\ }{ }
\newenvironment{examquestion}[4] {\hrulefill ~\\ \textbf{Question #1: #2 #3 } \hfill \grader \\ ~ \\ }{ }
\newenvironment{question}[3] {\hrulefill ~\\ \textbf{Q#1: #2 (#3) } \hfill \grader \\ ~ \\ }{ }
\newenvironment{simplequestion}[3] {\hrulefill ~\\ \textbf{Q#1: #2 } \\ ~ \\ }{ }

% Arguments:    #1 = question number
% This one won't show a weight or point value associated; I use this
% on the practice reviews or things I don't grade.
\newenvironment{questionNOGRADE}[1] {\hrulefill \paragraph{Question #1} ~\\}{}

\newenvironment{assignmentNOGRADE}[1] {\hrulefill \paragraph{Assignment #1} ~\\}{}

% Deprecated, but kept so I don't break my old exams
\newenvironment{answersheetquestion}[3] {\textbf{\footnotesize{#3\%}} Question #1: #2 \hfill \grader ~\\ }{ }

% Example:
% \stepcounter{question}
% \questionNOGRADE{\thequestion}

% \stepcounter{question}
% \examquestion{\thequestion}{Combinatorics}{}{+4}

% \stepcounter{question}
% \begin{examquestion}{\thequestion}{Name}{}
% \end{examquestion}

% At the beginning of exams I always list the question weights
% and use the table to write down the scores (0 - 4) to calculate.

\newcommand\gradetable[2]{ \\ \hline #1           & #2\%       & }
%\gradetable{questionnumber}{questionweight}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Symbols %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \diamondsuit, \heartsuit, \clubsuit, \spadesuit

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Programming assignments %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Code example frame %
\lstdefinestyle{code}{
    language=C++,
    showspaces=false,
    showstringspaces=false,
    tabsize=4,
    basicstyle=\ttfamily\small\color{textz}, 
    commentstyle=\ttfamily\color{commentz},
    keywordstyle=\ttfamily\color{keywordz},
    backgroundcolor=\color{backgroundz},
    breakatwhitespace=false,
    frame=single,
    keepspaces=true,
    breaklines=true,
    stepnumber=1,
    numbers=left,
    }

% Code example frame %
\lstdefinestyle{pycode}{
    language=Python,
    showspaces=false,
    showstringspaces=false,
    tabsize=4,
    basicstyle=\ttfamily\small\color{textz}, 
    commentstyle=\ttfamily\color{commentz},
    keywordstyle=\ttfamily\color{keywordz},
    backgroundcolor=\color{backgroundz},
    breakatwhitespace=false,
    frame=single,
    keepspaces=true,
    breaklines=true,
    stepnumber=1,
    numbers=left,
    }

% Example program output frame %
\lstdefinestyle{output}{
    showspaces=false,
    showstringspaces=false,
    tabsize=4,
    basicstyle=\ttfamily\small\color{previewfg}, 
    backgroundcolor=\color{previewbg},
    breakatwhitespace=false,
    frame=single,
    keepspaces=true,
    breaklines=true,
    %title=\tiny Example output,
    }

% Example text output frame %
\lstdefinestyle{textfile}{
    showspaces=false,
    showstringspaces=false,
    tabsize=4,
    basicstyle=\ttfamily\footnotesize\color{textfg}, 
    backgroundcolor=\color{textbg},
    breakatwhitespace=false,
    frame=single,
    keepspaces=true,
    breaklines=true,
    }


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Layout %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{sidebyside}[2]
{
    \begin{figure}[h]
        \centering
        \begin{subfigure}{.5\textwidth}
               #1
        \end{subfigure}%
        \begin{subfigure}{.5\textwidth}
               #2
        \end{subfigure}
    \end{figure}
}
% \sidebyside{a}{b}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Reference %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Write example code: %

% \begin{lstlisting}[style=code]
% \end{lstlisting}

% Write example program output (terminal): %

% \begin{lstlisting}[style=output]
% \end{lstlisting}

% Write example text file output: %

% \begin{lstlisting}[style=textfile]
% \end{lstlisting}

% Directory trees are useful but ugly. Here's an example of using it:

%\begin{framed}
%\dirtree{%
%.1 PROJECT/.
%.2 assetsManager{.}py.
%.3 audio.
%}
%\end{framed}

% Float two columns side-by-side. I usually avoid this because the
% LaTeX code is so ugly:

%\begin{figure}[h]
%    \centering
%    \begin{subfigure}{.5\textwidth}
%        \centering
%           Thing1
%    \end{subfigure}%
%    \begin{subfigure}{.5\textwidth}
%        \centering
%           Thing2
%    \end{subfigure}
%\end{figure}
