\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {Sets: Set Basics}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 3.1}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 1.1}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Set notation and basics}

    \begin{intro}{What is a set?}
        A set is a structure that contains information. Often,
        our sets will contain numbers, but they could also contain
        a list of colors, students, and so on. The items that are
        contained in a set are known as the \textbf{elements} of the set.

        \paragraph{Notation:} Sets are usually given single, capital letters
        as the names. Then, to specify the elements within the set,
        you write it between opening and closing curly-braces \{ \},
        with each element separated by a comma.

        \begin{center}
            $A = \{ 1, 2, 3, 4 \}$
        \end{center}
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Define the following sets...

        \begin{enumerate}
            \item[a.]   The set $G$ of all students in your group.
                        \notonkey{ \vspace{1cm} }{}
            \item[b.]   The set $C$ of all classes \underline{one} member of the group is taking.
                        \notonkey{ \vspace{1cm} }{}
        \end{enumerate}
    \end{questionNOGRADE}

    
    \newpage
    \begin{intro}{More info about sets}
        \textbf{Order doesn't matter:} Two sets with the same elements
        but a different order are treated as the same.

        \paragraph{Duplicates don't matter:} If an element shows up in a
        set more than once, it is still considered the same set as if
        it had just had one of that element.
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Given the sets:

        \begin{center}
            $A = \{ 1, 2, 3 \}$ \tab $B = \{ 2, 2, 3 \}$ \tab
            $C = \{ 5, 6, 7 \}$ \tab $D = \{ 3, 2, 1\} $
        \end{center}

        For the following, fill in whether the two sets are
        \textbf{equal} (=) or \textbf{not} ($\neq$). ~\\

        \begin{center}
            $A$ \fitb $B$ \tab[2cm] $A$ \fitb $C$ \tab[2cm] $A$ \fitb $D$
        \end{center}
    \end{questionNOGRADE}

    \hrulefill
        
    \begin{intro}{Common sets of numbers}
        Some common sets we will be working with are...
        ~\\
        $\mathbb{Z}$, \footnotesize the set of integers; whole numbers - positive, negative, and 0. \normalsize
        ~\\
        $\mathbb{N}$, \footnotesize the set of natural numbers; counting numbers - 0 and positive integers. \normalsize
        ~\\
        $\mathbb{Q}$, \footnotesize the set of rational numbers; any number that can be written as a fraction/ratio. \normalsize
        ~\\
        $\mathbb{R}$, \footnotesize the set of real numbers; all of the above, plus numbers with unending strings of digits after the decimal point. \normalsize
    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following numbers, check $\checkmark$ which set(s) they belong to.
        \large

        \begin{center}
            \begin{tabular}{| c | p{1cm} | p{1cm} | p{1cm} | p{1cm} |} \hline
                & $\mathbb{N}$ & $\mathbb{Z}$ & $\mathbb{Q}$ & $\mathbb{R}$ \\ \hline
                10      & & & &    \\ \hline
                -5      & & & &    \\ \hline
                12/6    & & & &    \\ \hline
                $\pi$   & & & &    \\ \hline
                2.40    & & & &    \\ \hline
            \end{tabular}
        \end{center}
    \end{questionNOGRADE}

    \newpage
    \subsection*{Subsets}

    \begin{intro}{Subsets}
        We can use $=$ to show that two sets are equivalent,
        but it can also be useful to show that one set is a \textbf{subset}
        of another - that is, all elements from one set are present in a second set.

        ~\\
        $A \subseteq B$ specifies that all elements of set $A$ are present in set $B$.

        ~\\
        If $A \subseteq B$ and $B \subseteq A$, then $A = B$.

        \paragraph{Element of...} ~\\
        You can specify that a number (or item) is \textbf{an element of} some set by
        using the $\in$ (``in") notation. For example, $x \in A$ means ``x in A".
    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For each scenario, check any items that are \textbf{true}. 
        \begin{enumerate}
            \item[a.]   $A = \{ 1, 2, 3 \}$ and $B = \{2, 3\}$ ~\\
                        \Square\ $A \subseteq B$        \tab
                        \Square\ $A \not\subseteq B$    \tab
                        \Square\ $B \subseteq A$        \tab
                        \Square\ $B \not\subseteq A$    \tab
                        \Square\ $A = B$ 

            \item[b.]   $C = \{ red, green, blue \}$ and $D = \{ red, yellow, blue \}$ ~\\
                        \Square\ $C \subseteq D$        \tab[0.7cm]
                        \Square\ $C \not\subseteq D$    \tab[0.7cm]
                        \Square\ $D \subseteq C$        \tab[0.7cm]
                        \Square\ $D \not\subseteq C$    \tab[0.7cm]
                        \Square\ $C = D$ 

            \item[c.]   $E = \{ 1, 1, 2, 3, 4 \}$ and $F = \{ 4, 3, 2, 2, 1 \}$ ~\\
                        \Square\ $E \subseteq F$        \tab[0.7cm]
                        \Square\ $E \not\subseteq F$    \tab[0.7cm]
                        \Square\ $F \subseteq E$        \tab[0.7cm]
                        \Square\ $F \not\subseteq E$    \tab[0.7cm]
                        \Square\ $E = F$ 
        \end{enumerate}
    \end{questionNOGRADE}

    \hrulefill
    \begin{intro}{Set cardinality}
        \textbf{Set cardinality} is a way of saying the ``size of the set'',
        or the ``amount of elements in the set''. It can be denoted as $|X|$ or $n(X)$,
        depending on the book.
    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Give the set cardinality for each.
        ~\\
        a. $A = \{ 1, 2, 3 \}$  \tab[1.1cm]
            $|A| = $
        ~\\~\\
        b. $B = \{ \}$  \tab[2cm]
            $|B| = $
    \end{questionNOGRADE}

    \newpage
    \subsection*{Set operations}

    \begin{intro}{Set operations}
        We can figure out the relationship between two sets with the following operations.

        \paragraph{Union $\cup$:} Given two sets $A$ and $B$, the union $A \cup B$ will
        be a set that contains all elements of both sets.

        \paragraph{Intersection $\cap$:} Given two sets $A$ and $B$, the intersection $A \cap B$
        will be a set that contains only the elements in common between both.

        \paragraph{Difference $-$:} Given two sets $A$ and $B$, the difference $A - B$ will
        be all the elements of $A$, EXCEPT for any elements that are also in $B$.
    \end{intro}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Given the sets:
        \begin{center}
            $A = \{ 1, 2, 3 \}$ \tab
            $B = \{ 2, 4, 6 \}$ \tab
            $C = \{ 6, 7 \}$
        \end{center}
        Solve each of the following operations:
        \begin{enumerate}
            \item[a.]   $A \cup B = $       \notonkey{ \vspace{0.7cm} }{}
            \item[b.]   $A \cap B = $       \notonkey{ \vspace{0.7cm} }{}
            \item[c.]   $A \cup C = $       \notonkey{ \vspace{0.7cm} }{}
            \item[d.]   $A \cap C = $       \notonkey{ \vspace{0.7cm} }{}
            \item[e.]   $A - B = $          \notonkey{ \vspace{0.7cm} }{}
            \item[f.]   $(A - B) \cup C = $ \notonkey{ \vspace{0.7cm} }{}
        \end{enumerate}
    \end{questionNOGRADE}

    \newpage
    \begin{intro}{Complement, Universe, and Empty Set}
        When we're working with Set problems, usually a Universal set
        will be specified. The \textbf{Universal Set} $U$ is the set
        of all elements of all sets in the problem (and possibly additional elements).

        ~\\
        The \textbf{Empty Set} $\emptyset$ is a set with no elements contained
        within it. If you take the intersection of two sets with no common elements,
        the result will be $\emptyset$.

        ~\\
        The \textbf{Complement} of a set $X$, written as $\bar{X}$ or $X'$,
        will be $U - X$; the set of everything in the universe, EXCEPT for
        any elements in $X$.
    \end{intro}
    
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Given the sets:
        \begin{center}
            $A = \{ 1, 2, 3 \}$ \tab
            $B = \{ 2, 4, 6 \}$ \tab
            $C = \{ 5, 6 \}$
            
            $U = \{ 1, 2, 3, 4, 5, 6, 7 \}$
        \end{center}
        Solve each of the following operations:
        \begin{enumerate}
            \item[a.]   $A' = $             \notonkey{ \vspace{0.7cm} }{}
            \item[b.]   $U-A = $            \notonkey{ \vspace{0.7cm} }{}
            \item[c.]   $C - B = $          \notonkey{ \vspace{0.7cm} }{}
            \item[d.]   $A \cap C = $       \notonkey{ \vspace{0.7cm} }{}
            \item[e.]   $(A \cup B)' = $    \notonkey{ \vspace{0.7cm} }{}
            \item[e.]   $A' \cap B' = $     \notonkey{ \vspace{0.7cm} }{}
        \end{enumerate}
    \end{questionNOGRADE}
    
    \newpage
    \subsection*{Venn diagrams}
    \begin{intro}{Venn diagrams}
        We can visualize a general set problem by using Venn diagrams.
        A Venn diagram will contain a rectangle, the \textbf{Universe},
        and circles to represent each set. Regions are shaded in to
        show what parts of what sets are included.

        \def\circleA{(1.5,1.5) circle (1.0cm)}
        \def\circleB{(2.5,1.5) circle (1.0cm)}

        \begin{center}
            \begin{tabular}{c c}
                $A$
                &
                $A'$
                \\
                \begin{tikzpicture}
                    \draw[fill=white] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
                    \draw[fill=orange] (1.5, 1.5) circle (1cm) node[anchor=south east] {$A$};
                \end{tikzpicture}
                &
                \begin{tikzpicture}
                    \draw[fill=orange] (0,0) -- (3,0) -- (3,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
                    \draw[fill=white] (1.5, 1.5) circle (1cm) node[anchor=south east] {$A$};
                \end{tikzpicture}
                \\ \\
                $A \cap B$
                &
                $A \cup B$
                \\
                \begin{tikzpicture}
                    \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
                    \begin{scope}
                        \clip \circleA;
                        \fill[fill=orange] \circleB;
                    \end{scope}
                    \draw \circleA node[anchor=south east] {$A$};
                    \draw \circleB node[anchor=south west] {$B$};
                \end{tikzpicture}
                &                
                \begin{tikzpicture}
                    \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
                    \draw[fill=orange]  \circleA node[anchor=south east] {$A$}
                                        \circleB node[anchor=south west] {$B$};
                \end{tikzpicture}
                \\ \\
                $(A \cap B)'$
                &
                $A - B$
                \\
                \begin{tikzpicture}
                    \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
                    \draw[fill=orange, even odd rule]  \circleA node[anchor=south east] {$A$}
                                        \circleB node[anchor=south west] {$B$};
                \end{tikzpicture}
                &
                \begin{tikzpicture}
                    \draw[fill=white] (0,0) -- (4,0) -- (4,3) -- (0,3) -- (0,0) node[anchor=south west] {$U$};
                    \begin{scope}
                        \clip \circleA;{}
                        \draw[fill=orange, even odd rule]   \circleA node[anchor=south east] {$A$}
                                                            \circleB;
                    \end{scope}
                    \draw[]             \circleA node[anchor=south east] {$A$}
                                        \circleB node[anchor=south west] {$B$};
                \end{tikzpicture}
                
            \end{tabular}
        \end{center}
    \end{intro}
    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Fill in the Venn diagrams for the following operations:

        \begin{center}
            \begin{tabular}{l l l}
                a.      $A \cup B$
                & b.    $A \cup C$
                & c.    $A \cup B \cup C$
                \\
                \venndiagram &
                \venndiagram &
                \venndiagram
                \\ \\
                d.      $(A \cup B) - C$
                & e.    $(A \cap B) \cup C$
                & f.    $A - (B \cap C)$
                \\
                \venndiagram &
                \venndiagram &
                \venndiagram
                \\ \\
                g.      $A'$
                & h.    $(A \cup B)'$
                & i.    $(A \cap B \cap C)'$
                \\
                \venndiagram &
                \venndiagram &
                \venndiagram
            \end{tabular}
        \end{center}
    \end{questionNOGRADE}
    
}{
% KEY ------------------------------------ %

    \begin{enumerate}
        \item
            \begin{itemize}
                \item[a.]   asdf
                \item[b.]   asdf
                \item[c.]   asdf
            \end{itemize}
    \end{enumerate}

}

\input{../BASE-4-FOOT}
