#include <iostream>
using namespace std;

void swap( int& a, int& b )
{
    int c = a;
    a = b;
    b = c;
}

int gcd( int a, int b )
{
    cout << "gdc( " << a << ", " << b << " )..." << endl;
    int r;
    if ( a < b ) {
        cout << "\t a is greater than b, swap!" << endl;
        swap( a, b );
    }

    cout << "\t while b != 0... (b = " << b << ")" << endl;
    while ( b != 0 )
    {
        cout << "\n\t\t Loop" << endl;
        cout << "\t\t a = " << a << "\t b = " << b << endl;
        
        r = a % b;
        cout << "\t\t r = a % b (r = " << r << ")" << endl;
        
        a = b;
        cout << "\t\t a = b (a = " << a << ")" << endl;
        
        b = r;
        cout << "\t\t b = r (b = " << b << ")" << endl;
    }

    cout << "\t Return a (a = " << a << ")" << endl;
    return a;
}

int main()
{
    while ( true )
    {
        int a, b;
        cout << "Enter a and b: ";
        cin >> a >> b;
        int d = gcd( a, b );

        cout << "Result: " << d << endl << endl;
    }
    
    return 0;
}
