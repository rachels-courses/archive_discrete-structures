def swap( a, b ):
    c = a
    a = b
    b = c

def gcd( a, b ):
    print( "gcd( " + str(a) + ", " + str(b) + ")" )
    
    if ( a < b ):
        print( "\t swap( " + str(a) + ", " + str(b) + ")" )
        swap( a, b )

    print( "\t While loop..." )
    while ( b is not 0 ):
        print( "\t\t a = " + str(a) + ", b = " + str(b) )

        r = a % b
        print( "\t\t\t r = " + str(a) + "%" + str(b) + " = " + str(r) )
        
        a = b
        print( "\t\t\t a = b; a = " + str(a) )

        b = r
        print( "\t\t\t b = r; b = " + str(b) )

    print( "\t Result, return a; a = " + str(a) )
    return a


while ( True ):
    a = input( "Enter a: " )
    b = input( "Enter b: " )

    d = gcd( a, b )
 
    print( "\n\n" )
