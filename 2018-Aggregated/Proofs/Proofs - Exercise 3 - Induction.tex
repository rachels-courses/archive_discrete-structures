\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {Proofs: Mathematical Induction}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 2.3}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 2.4}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    %------------------------------------------------------------------%
    \subsection*{Sequences}
    
    \begin{intro}{\ }
        \textbf{Definition: Recursive formula} (aka recurrence relation) \\
        In mathematics, a recurrence relation is an equation that recursively defines
        a sequence [...] of values, once one or more initial terms are given:
        each further term of the sequence [...] is defined as a function
        of the preceding terms.
        \footnote{From https://en.wikipedia.org/wiki/Recurrence\_relation}

        \paragraph{Definition: Closed formula} ~\\
        A closed formula for a sequence is a formula where each term is described
        only in relation to its position in the list.
        \footnote{From Discrete Mathematics Mathematical Reasoning and Proof with Puzzles, Patterns, and Games by Douglas E Ensley}

        \paragraph{Definition: Sequence notation}
        Sequence notation is where we have some sequence, $a$,
        and $a_{n}$ denotes the element at position $n$. On a computer,
        the subscript may be written as \texttt{a[n]}.
    \end{intro}

    \newpage
    % - QUESTION --------------------------------------------------%
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Write out the first 5 elements of the following equations: \\
        \begin{itemize}
            \item[a.]   The closed formula $a_{n} = n+1$
                        \\
                        \inlinehint{$a_{1} = $ \tab[2cm] $a_{2} = $ \tab[2cm] $a_{3} = $ \tab[2cm]$a_{4} = $ \tab[2cm] $a_{5} = $}
            \item[b.]   The closed formula $a_{n} = 2n+1$                           \vspace{1cm}
            \item[c.]   The recursive formula $a_{1} = 1, a_{n} = a_{n-1} + 2$ \\
                        \inlinehint{$a_{1} = 1$ \tab[2cm] $a_{2} = a_{1} + 2 = $ \tab[2cm] $a_{3} = a_{2} + 2 = $  \tab[2cm] (etc)}    
                        \vspace{1cm}
            \item[d.]   The resursive formula $a_{1} = 2, a_{n} = 2 a_{n-1} + 1$    \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    % - QUESTION --------------------------------------------------%
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For each equation, plug $m-1$ in as $n$ and simplify.
        \begin{itemize}
            \item[a.]   $a_{n} = n+1$   \tab \inlinehint{Example: $a_{m-1} \tab = (m-1) + 1 \tab = m$}
            \vspace{1cm}
            \item[b.]   $a_{n} = 2n+1$                        \vspace{1cm}
            \item[c.]   $a_{1} = 1, a_{n} = a_{n-1} + 2$      \vspace{1cm}
            \item[d.]   $a_{1} = 2, a_{n} = 2 a_{n-1} + 1$    \vspace{1cm}
        \end{itemize}
    \end{questionNOGRADE}

    \newpage
    
    \subsection*{Summations}
    \begin{intro}{\ }
        For a sequence of numbers (denoted $a_{k}$, where $k >= 1$,
        we can use the notation
        $$\sum_{k=1}^{n} a_{k}$$
        to denote the sum of the first $n$ terms of the sequence.
        This is called \textit{sigma notation}.

        \paragraph{Example:} Evaluate the sum $\sum_{k=1}^{3}(2k-1)$. ~\\
        First, we need to find the elements at $k=1$, $k=2$, and $k=3$:

        \begin{center}
            \begin{tabular}{| c | c | c |}
                \hline
                \textbf{ $k=1$ } & \textbf{ $k=2$ } & \textbf{ $k=3$ } \\
                \hline
                $a_{1} = (2 \cdot 1 - 1) = 1$ &
                $a_{2} = (2 \cdot 2 - 1) = 3$ &
                $a_{3} = (2 \cdot 3 - 1) = 5$
                \\
                \hline
            \end{tabular}
        \end{center}
        ~\\
        Then, we can add the values: \\
        $\sum_{k=1}^{3}(2k-1) $ \tab
        $= a_{1} + a_{2} + a_{3}$ \tab
        $= 1 + 3 + 5 $ \tab
        \fbox{ $= 9$ }
    \end{intro}


    % - QUESTION --------------------------------------------------%
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Evaluate the following summations.

        \begin{itemize}
            \item[a.]   $ \sum_{k=1}^{4}(3k) $   ~\\
                        \begin{tabular}{l | l l l l l}
                            & $k=1$ & $k=2$ & $k=3$ & $k=4$ & $k=5$ \\ \hline
                            Value of $3k$ &
                        \end{tabular} ~\\ ~\\

                        Resulting sum:

                        \vspace{2cm}
                        
            \item[b.]   $ \sum_{k=1}^{5}(4) $ ~\\
                        \begin{tabular}{l | l l l l l}
                            & $k=1$ & $k=2$ & $k=3$ & $k=4$ & $k=5$ \\ \hline
                            Value of $4$ &
                        \end{tabular} ~\\ ~\\

                        Resulting sum:
        \end{itemize}
    
    \end{questionNOGRADE}
    
    \newpage
    \subsection*{Recursive / Closed formula equivalence}

    \begin{intro}{Example} \small
        Show that the sequence defined by the \textbf{recursive formula}
        \footnote{From Discrete Mathematics, Ensley and Crawley} ~\\
        $$a_{k} = a_{k-1} + 4; a_{1} = 1$$ for $k \geq 2$
        is equivalently described by the \textbf{closed formula} $$a_{n} = 4n - 3$$

        \hrulefill
        
        \paragraph{Basis Step: Check $a_{1}$ for both formulas.} ~\\ ~\\
            Recursive: $a_{1} = 1$ (provided); \tab
            Closed: $a_{1} = 4(1) - 3 = 1$  \tab \checkmark OK

        \hrulefill

        \paragraph{\small Inductive Step: Show that this is true for all values up through $n-1$:}

        \subparagraph{\footnotesize Find an equation for $a_{k-1}$ via the closed formula provided:} ~\\

        Original proposition:   $a_{n} = 4n - 3$ and $a_{k} = a_{k-1} + 4; a_{1} = 1$ are equivalent. Use $a_{n} = 4n - 3$ to find a value for $a_{k-1}$. ~\\

        \begin{tabular}{p{5cm} l}
            1. $a_{n} = 4n - 3$         & The closed formula   \\
            2. $a_{k-1} = 4(k-1) - 3$   & Plugging in $k-1$ into $n$ \\
            3. $a_{k-1} = 4k - 4 - 3$   & Simplifying... \\
            4. $a_{k-1} = 4k - 7$       & Simplified.
        \end{tabular}

        \subparagraph{\footnotesize Plug the equation for $a_{k-1}$ into the recursive formula and simplify.} ~\\
        
        \begin{tabular}{p{5cm} l}
            1. $a_{k} = a_{k-1} + 4$                    & The recursive formula \\
            2. $a_{k} = 4k - 7 + 4$                     & Plugging in $a_{k-1} = 4k - 7$. \\
            3. \framebox[1.1\width]{$a_{k} = 4k - 3$}   & Simplified to the original form.
        \end{tabular} ~\\~\\

        We have manipulated the \textbf{recursive formula} to end up with the
        same \textbf{closed formula} as stated in the original proposition,
        therefore we have shown that they are equivalent.

    \end{intro}
    
    \newpage

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Show that the sequence defined by
            $a_{n} = a_{n-1} + 2; a_{1} = 5$ for $k \geq 2$ is equivalently
            described by the closed formula, $a_{n} = 2n+3$. \footnote{From Discrete Mathematics by Ensley and Crawley}

        \paragraph{Basis Step: Check $a_{1}$ for both formulas.} ~\\
            \vspace{2cm}
            
        \paragraph{Inductive Step:} ~\\
            \subparagraph{Find the equation for $a_{n-1}$ via the closed formula:} ~\\
            \vspace{4cm}
            
        \subparagraph{Plug $a_{n-1}$ back into the recursive formula and simplify.} ~\\
            \vspace{3cm}
        \end{questionNOGRADE}
    \newpage

        \begin{hint}{Exponent rules}
            \textbf{Power rule:} $(a^{m})^{n} = a^{mn}$ \\
            \textbf{Negative exponent rule:} $a^{-n} = \frac{1}{a^{n}}$ \\
            \textbf{Product rule:} $a^{m} \cdot a^{n} = a^{m+n}$ \\
            \textbf{Quotient rule:} $\frac{a^{m}}{a^{n}} = a^{m-n}$
        \end{hint}
        
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Show that the sequence defined by $b_{k} = 4 \cdot b_{k-1} + 3, b_{1} = 3$ for $k \geq 2$,
            is equivalently described by the closed formula $b_{n} = 2^{2n} - 1$. \footnote{From Discrete Mathematics by Ensley and Crawley}

        \paragraph{Basis Step: Check $b_{1}$ for both formulas.} ~\\
            \vspace{2cm}
            
        \paragraph{Inductive Step:} ~\\
            \subparagraph{Find the equation for $b_{k-1}$ via the closed formula:} ~\\
            \vspace{4cm}
            
        \subparagraph{Plug $b_{k-1}$ back into the recursive formula and simplify.} ~\\
            \vspace{3cm}
        \end{questionNOGRADE}

    \newpage
    \subsection*{Sum / Closed formula equivalence}

    \begin{intro}{Example} \footnotesize
    
        Use induction to prove the proposition. As part of the proof,
        verify the statement for $n = 1$, $n = 2$, and $n = 3$.
        $\sum_{i=1}^{n} (2i - 1) = n^{2} $        for each $n \geq 1$. \footnote{From Discrete Mathematics by Ensley and Crawley}

        \hrulefill

        \paragraph{Basis step: Show that the proposition is true for 1, 2, and 3. } ~\\

            \begin{tabular}{l l l} 
                \scriptsize $i=1$: & \scriptsize LHS: $ \sum_{i=1}^{1} (2i - 1) = ( 2 \cdot 1 - 1 ) = 1$ & \scriptsize RHS: $1^{2} = 1$ \checkmark
                \\
                \scriptsize $i=2$: & \scriptsize LHS: $ \sum_{i=1}^{2} (2i - 1) = (2 \cdot 1 - 1) + (2 \cdot 2 - 1) = (1) + (3) = 4 $ & \scriptsize RHS: $2^{2} = 4$ \checkmark
                \\
                \scriptsize $i=3$: & \scriptsize LHS: $\sum_{i=1}^{3} (2i - 1) = 1 + 3 + (2 \cdot 3 - 1) = 1 + 3 + 5 = 9 $ & \scriptsize RHS: $3^{2} = 9$ \checkmark
            \end{tabular} \footnote{LHS = left-hand side, RHS = right-hand side} ~\\

        \hrulefill
        
        \paragraph{Inductive Step} ~\\
        \subparagraph{The sum is equivalent to the sum up until $n-1$, plus the final term at $i=n$:} ~\\

        \begin{tabular}{p{7cm} p{4.2cm}}
            1. $\sum_{i=1}^{n}(2i-1)$ & The original sum. \\ \\
            2. $\sum_{i=1}^{n}(2i-1) = \sum_{i=1}^{n-1}(2i-1) + (2n - 1)$ & \footnotesize The sum is equivalent to the sum from $i=1$ to $n-1$, plus the final $i=n$. \\
        \end{tabular}

        \subparagraph{Find an equation for $\sum_{i=1}^{n-1}$ from the original proposition:} ~\\
        
        \begin{tabular}{p{7cm} p{4cm}}
            1. $\sum_{i=1}^{n} (2i - 1) = n^{2}$ & Original proposition. \\
            2. $\sum_{i=1}^{n-1} (2i - 1) = (n-1)^{2}$ & Plugging in $n-1$. \\
            3. $\sum_{i=1}^{n-1} (2i - 1) = n^{2} - 2n + 1$ & Simplified. \\
        \end{tabular}

        \subparagraph{Plug $\sum_{i=1}^{n-1}$ into the equation for the sum made previously:} ~\\
        
        \begin{tabular}{p{7cm} p{4cm}}
            1. $\sum_{i=1}^{n}(2i-1) = \sum_{i=1}^{n-1}(2i-1) + (2n - 1)$ & Our sum formula. \\
            2. $\sum_{i=1}^{n}(2i-1) = (n^{2} - 2n + 1) + (2n - 1)$ & plugged in $\sum_{i=1}^{n-1} (2i - 1) = n^{2} - 2n + 1$. \\
            3. \framebox[1.1\width]{$\sum_{i=1}^{n}(2i-1) = n^{2}$} & Simplified to the original proposition. \\
        \end{tabular} ~\\

        We get the same form as the original proposition, proving our statement.
    \end{intro}


    \newpage

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion} \footnotesize

            Use induction to prove
            $$ \sum_{i=1}^{n} (2i+4) = n^{2} + 5n$$
            for each $n \geq 1$.

        \paragraph{Basis Step: Show that the proposition is true for 1, 2, and 3. } ~\\

            \begin{tabular}{l | p{6cm} | p{4cm} }
                \footnotesize \textbf{ $i$ value } &
                \footnotesize \textbf{ $ \sum_{i=1}^{n} (2i+4) $ } &
                \footnotesize \textbf{ $n^{2} + 5n$ }
                \\ \hline
                \footnotesize $i = 1$ & & \\ 

                \footnotesize $i = 2$ & & \\ 

                \footnotesize $i = 3$ & & \\
            \end{tabular} ~\\

        \hrulefill

        \paragraph{Inductive Step:}
        \subparagraph{\scriptsize Make an equation for the sum that is the sum up to $n-1$, plus the final term:} ~\\ \vspace{1cm}

        \subparagraph{\scriptsize Find an equation  for $\sum_{i=1}^{n-1}$ via the proposition: }  ~\\ \vspace{2cm}

        \subparagraph{\scriptsize Plug $\sum_{i=1}^{n-1}$ into the sum equation and simplify:} ~\\ \vspace{3cm}

        \end{questionNOGRADE}

        \newpage

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion} \footnotesize

            Use induction to prove that for every positive integer $n$,
            $$ \sum_{i=1}^{n} i = \frac{n(n+1)}{2}$$

        \paragraph{Basis Step: Show that the proposition is true for 1, 2, and 3. } ~\\

            \begin{tabular}{l | p{6cm} | p{4cm} }
                \footnotesize \textbf{ $i$ value } &
                \footnotesize \textbf{ $ \sum_{i=1}^{n} (2i+4) $ } &
                \footnotesize \textbf{ $n^{2} + 5n$ }
                \\ \hline
                \footnotesize $i = 1$ & & \\ 

                \footnotesize $i = 2$ & & \\ 

                \footnotesize $i = 3$ & & \\
            \end{tabular} ~\\

        \hrulefill

        \paragraph{Inductive Step:}
        \subparagraph{\scriptsize Make an equation for the sum that is the sum up to $n-1$, plus the final term:} ~\\ \vspace{1cm}

        \subparagraph{\scriptsize Find an equation  for $\sum_{i=1}^{n-1}$ via the proposition: }  ~\\ \vspace{2cm}

        \subparagraph{\scriptsize Plug $\sum_{i=1}^{n-1}$ into the sum equation and simplify:} ~\\ \vspace{3cm}
        
        \end{questionNOGRADE}
    
    

}
{
% KEY ------------------------------------ %

    \begin{enumerate}
        \item
            \begin{itemize}
                \item[a.]   $a = 2k$, $b = 2j+1$ \tab $a+b \equiv 2k + 2j+1$ \tab $= 2(k + j) + 1$.
            \end{itemize}
            
        \item
            \begin{itemize}
                \item   $x$ and $y$ are two integers whose product is odd.
                \item   Both $x$ and $y$ are odd.
                \item   If both $x$ and $y$ are not odd, then the product of $x$ and $y$ is not odd.
                \item   $x = 2k$
                \item   $y = 2j$
                \item   $(2k)(2j) = 2(2kj)$
            \end{itemize}

        \item
            \begin{itemize}
                \item   $n$ is an odd integer
                \item   $n^{2} + n$ is even
                \item   $n$ is an odd integer and $n^{2} + n$ is not even.
                \item   $n = 2k+1$
                \item   $n^{2} + n = 2j+1$
                \item   $(2k+1)^{2} + (2k+1) = 2j+1$ ~\\
                        $4k^{2} + 4k + 1 + 2k + 1 = 2j + 1$ ~\\
                        $4k^{2} + 6k - 2j = 1 - 1 - 1$ ~\\
                        $4k^{2} + 6k - 2j = -1$ ~\\
                        $2(2k^{2} + 3k - j) = -1$ ~\\
                        $2k^{2} + 3k - j = \frac{-1}{2}$
            \end{itemize}
    \end{enumerate}

}

\input{../BASE-4-FOOT}
