\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {Proofs: Proof by Contradiction, Proof by Contrapositive}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 2.1, 2.5}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 2.2}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

\notonkey{
% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

        \begin{intro}{Review: Direct proofs}
            With a Direct Proof, you take a statement like,
            \textit{``The sum of $n$ times $n+1$ is an even number.''}
            and replace the variables with definitions for
            the relevant type: $n$ becomes $2k+1$ and we can write
            $(2k+1)(2k+1+1)$. From there, we simplify until we get
            the form of the definition of an even integer (per this
            example's statement.)
        \end{intro}

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Prove the following statement using a \underline{direct proof}.

            \textit{``For all integers $a$ and $b$, if $a$ is even and $b$ is odd, then $a+b$ is odd.''}

            Start with $a = 2k$ and $b = 2j+1$.

            \begin{hint}{Hint!}
                Remember that we're not doing proof by contradiction. We start with equations for $a$ and $b$,
                plug these equations into $a + b$, and then simplify and factor into the definition for an odd number.
            \end{hint}
        \end{questionNOGRADE}
    
    \newpage
    \begin{intro}{Proof by Contrapositive} \small
        Given some statement, $p \to q$, the contrapositive, $\neg q \to \neg p$ is
        \textbf{logically equivalent}. We can take advantage of this to perform proofs.

        \paragraph{Example:} Prove that, for all integers $n$, if $n^{2}$ is even, then $n$ is even.
        \footnote{Discrete Mathematics, Ensley and Crawley, p 94-95}

        ~\\
        The contrapositive here would be, ``if $n$ is not even, then $n^{2}$ is not even.''
        Then we can solve as a direct proof...

        \begin{enumerate}
            \item   $n$ is odd: $n = 2k+1$.
            \item   $n^{2} \equiv (2k+1)^{2}$, then simplify...
            \item   $(2k+1)^{2} = 4k^{2} + 4k + 1$, and factor into the form of an odd number...
            \item   \framebox[1.1\width]{$= 2(2k^{2} + 2k) + 1$.}
        \end{enumerate} \normalsize
    \end{intro}

    \newpage
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Prove the following with \underline{proof by contrapositive}...

            \begin{center}
                ``If $x$ and $y$ are two integers whose product is odd, then both must be odd.''\footnote{From http://zimmer.csufresno.edu/~larryc/proofs/proofs.contrapositive.html}
            \end{center}

            \begin{enumerate}
                \item   Write out the hypothesis $p$, in English: ~\\
                        \footnotesize \textit{Hint: $p$ doesn't include ``if''!} \vspace{1cm}
                \item   Write out the conclusion $q$, in English:       \vspace{1cm}
                \item   Write out the contrapositive $\neg q \to \neg p$, in English:   \vspace{1cm}
                \item   Give an equation for $\neg q$ (in the contrapositive):  \vspace{1cm}
                \item   Give an equation for $\neg p$ (in the contrapositive):  \vspace{1cm}
                \item   Solve:
            \end{enumerate}
        \end{questionNOGRADE}

    
    \newpage
    \begin{intro}{Proof by Contradiction}
        We can also prove statements by disproving the \textit{negation} of that statement.
        If we can disprove the negation, when we are proving the original statement.
        
        \paragraph{Example:} Prove that, if $n^{2}$ is even, then $n$ is even.

        Remember that the negation of $p \to q$ is $p \land \neg q$. Here,
        our negation would be $n^{2}$ is even and $n$ is not even. Then,
        we can ``translate'' this into symbols:


        \begin{itemize}
            \item   $n^{2}$ is even: \tab[1.4cm] $n^{2} = 2k$ (some even integer.)
            \item   $n$ is not even: \tab $n = 2j+1$ (some odd integer.)
        \end{itemize}
        
        As we solve, if we run into a \textbf{contradiction}, then we cannot
        prove the negation, and this shows the proof for the original statement.
        ~\\

            \begin{tabular}{l l}
                $(2j + 1)^{2} = 2k$ & \footnotesize $n = 2j+1$, so squaring it should give
                    \\ & us $n^{2}$, or some even integer.
                \\
                $\Rightarrow$ \tab[0.5cm] $4j^{2} + 4j + 1 = 2k$
                    & \footnotesize FOILing $(2j+1)^{2}$
                \\
                $\Rightarrow$ \tab[0.5cm] $1 = 2k - 4j^{2} - 4j$
                    & \footnotesize Move constants to one side
                \\
                $\Rightarrow$ \tab[0.5cm] $1 = 2(k - 2j^{2} - 2j)$
                    & \footnotesize Pull out common factor
                \\
                $\Rightarrow$ \tab[0.5cm] \framebox[1.1\width]{$\frac{1}{2} = k - 2j^{2} - 2j$}
                    & \footnotesize Divide both sides 
            \end{tabular}
            ~\\~\\

        Since $k$ and $j$ are both integers, through the closure property
        of integers (+, -, and $\times$ results in an integer),
        we can show that $k - 2j^{2} - 2j$ results in something that
        is \textit{not an integer} -- this is a contradiction.
        It shows that our counter-example
        is \textbf{false}, and no counter-example can exist.
    \end{intro}

    \newpage
    
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Prove the following with \underline{proof by contradiction}...

            \begin{center}
                ``If $n$ is an odd integer, then $n^{2} + n$ is even.'' \footnote{Discrete Mathematics, Ensley and Crawley, pg 133}
            \end{center}

            \begin{enumerate}
                \item   Write out the hypothesis $p$:       \vspace{1cm}
                \item   Write out the conclusion $q$:       \vspace{1cm}
                \item   Write out the negation $p \land \neg q$:         \vspace{1cm}
                \item   Give an equation for $p$        (in the negation):  \vspace{1cm}
                \item   Give an equation for $\neg q$   (in the negation):  \vspace{1cm}
                \item   Solve:
            \end{enumerate}
        \end{questionNOGRADE}
        

}
{
% KEY ------------------------------------ %

    \begin{enumerate}
        \item
            \begin{itemize}
                \item[a.]   $a = 2k$, $b = 2j+1$ \tab $a+b \equiv 2k + 2j+1$ \tab $= 2(k + j) + 1$.
            \end{itemize}
            
        \item
            \begin{itemize}
                \item   $x$ and $y$ are two integers whose product is odd.
                \item   Both $x$ and $y$ are odd.
                \item   If both $x$ and $y$ are not odd, then the product of $x$ and $y$ is not odd.
                \item   $x = 2k$
                \item   $y = 2j$
                \item   $(2k)(2j) = 2(2kj)$
            \end{itemize}

        \item
            \begin{itemize}
                \item   $n$ is an odd integer
                \item   $n^{2} + n$ is even
                \item   $n$ is an odd integer and $n^{2} + n$ is not even.
                \item   $n = 2k+1$
                \item   $n^{2} + n = 2j+1$
                \item   $(2k+1)^{2} + (2k+1) = 2j+1$ ~\\
                        $4k^{2} + 4k + 1 + 2k + 1 = 2j + 1$ ~\\
                        $4k^{2} + 6k - 2j = 1 - 1 - 1$ ~\\
                        $4k^{2} + 6k - 2j = -1$ ~\\
                        $2(2k^{2} + 3k - j) = -1$ ~\\
                        $2k^{2} + 3k - j = \frac{-1}{2}$
            \end{itemize}
    \end{enumerate}

}

\input{../BASE-4-FOOT}
