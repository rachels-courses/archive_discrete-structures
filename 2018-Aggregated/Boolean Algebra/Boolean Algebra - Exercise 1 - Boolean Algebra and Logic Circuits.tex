\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {Extra: Circuits}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 3.4, 3.5}
\newcommand{\laTextbookB}   {}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

% ASSIGNMENT ------------------------------------ %

    \section*{Boolean Algebra}

    \subsection*{Logic, Sets, and Boolean Algebra}

    \begin{intro}{\ }
        Logic ($p \land q$), sets ($P \cap Q$), and now boolean algebra ($p \cdot q$)
        have a lot in common with each other. In fact, sometimes it can be useful
        to convert a problem from one type to another in order to learn more about
        a problem. Here are the ``translations'':

        \begin{center}
            \begin{tabular}{l c c c}
                & & & \textbf{Boolean}
                \\
                & \textbf{Logic} & \textbf{Sets} & \textbf{Algebra}
                \\ \hline
                \textbf{Variables} &
                    $p$, $q$, $r$ & $A$, $B$, $C$ & $a$, $b$, $c$
                \\ \\
                \textbf{``and'' operation} &
                    $\land$ & $\cap$ & $\cdot$
                \\ \\
                \textbf{``or'' operation} &
                    $\lor$ & $\cup$ & $+$
                \\ \\
                \textbf{``not'' operation} &
                    $\neg$ & $'$ & $'$
                \\ \\
                \textbf{``-'' operation} &
                    $a \land \neg b$ & $A - B$ & $a \cdot b'$
                \\ \\
                \textbf{Special} &
                    Tautology & Universal set U & 1
                    \\ \\
                    & Contradiction & Empty set $\emptyset$ & 0
            \end{tabular}
        \end{center}
    \end{intro}

    \newpage

    \begin{intro}{Example:}
        Rephrase the following Logic operation using Set and Boolean Algebra notations: $ ( p \land q ) \lor r $

        \begin{itemize}
            \item   Logic:              $ ( p \land q ) \lor r $
            \item   Sets:               $(P \cap Q) \cup R$
            \item   Boolean algebra:    $( p \cdot q ) + r$
        \end{itemize}
    \end{intro}

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Rewrite the following using Boolean Algebra notation:

            \begin{enumerate}
                \item[a.]   $p \land q$                         \vspace{0.5cm} \solution{ $p \cdot q$ }{}
                \item[b.]   $p \lor q$                          \vspace{0.5cm} \solution{ $p + q$ }{}
                \item[c.]   $\neg p$                            \vspace{0.5cm} \solution{ $p'$ }{}
                \item[d.]   $(p \land \neg q) \lor p$           \vspace{0.5cm} \solution{ $(p \cdot q' ) + p$ }{}
                \item[e.]   $\neg(\neg p)$                      \vspace{0.5cm} \solution{ $(p')' = p$ }{}
                \item[f.]   $(p \land \neg q) \lor p \equiv p$  \vspace{0.5cm} \solution{ $(p \cdot q') + p = p$ }{}
                \item[g.]   $(A-B)$                             \vspace{0.5cm} \solution{ $a \cdot b'$ }{}
                \item[h.]   $A' \cup (A \cap B)$                \vspace{0.5cm} \solution{ $a' + (a \cdot b)$ }{}
                \item[i.]   $(A - B)' = A' \cup (A \cap B)$     \vspace{0.5cm} \solution{ $(a \cdot b')' = a' + (a \cdot b)$ }{}
            \end{enumerate}
        \end{questionNOGRADE}

    \newpage
    
    \subsection*{Boolean Algebra properties}

        \begin{intro}{Boolean Algebra Properties \footnote{From Discrete Mathematics, Ensley and Crawley}} ~\\
            \begin{tabular}{l | c | c}
                Commutative & $a \cdot b = b \cdot a$ & $a + b = b + a$ \\ & & \\
                Associative & $(a \cdot b) \cdot c = a \cdot (b \cdot c)$ & $(a + b) + c = a + (b + c)$ \\ & & \\
                Distributive & $a \cdot (b + c)$ & $a + (b \cdot c)$ \\
                & $= (a \cdot b) + (a \cdot c)$ & $= (a + b) \cdot (a + c)$ \\ & & \\
                Identity & $a \cdot 1 = a$ & $a + 0 = a$ \\ & & \\
                Negation & $a + a' = 1$ & $a \cdot a' = 0$ \\& &  \\
                Double negative & $(a')' = a$ \\ & & \\
                Idempotent & $a \cdot a = a$ & $a + a = a$ \\& &  \\
                DeMorgan's laws & $(a \cdot b)' = a' + b'$ & $(a + b)' = a' \cdot b'$ \\& &  \\
                Universal bound & $a + 1 = 1$ & $a \cdot 0 = 0$ \\& &  \\
                Absorption & $a \cdot (a + b) = a$ & $a + (a \cdot b) = a$ \\ & & \\
                Complements & $1' = 0$ & $0' = 1$ \\
                of 1 and 0
            \end{tabular}
        \end{intro}

    \newpage

        \begin{intro}{Examples \footnote{From Discrete Mathematics, Ensley \& Crawley, 3.4}}
            \textbf{Example 1:} Simplify $(a+1) \cdot (a+0)$ ~\\~\\
                \begin{tabular}{l l}
                    $ (a+1) \cdot (a+0) $ \\
                    $= 1 \cdot (a+0)$ & universal bound \\
                    $= (a+0) \cdot 1$ & commutative \\
                    $= a+0$ & identity \\
                    $= a$ & identity
                \end{tabular}

            \paragraph{Example 2:} Simplify $a \cdot (a' + b)$ ~\\~\\
                \begin{tabular}{l l}
                    $a \cdot (a' + b)$ \\
                    $= a \cdot a' + a \cdot b$ & distributive \\
                    $= 0 + a \cdot b$ & negation \\
                    $= a \cdot b + 0$ & commutative \\
                    $= a \cdot b$ & identity
                \end{tabular}

            \paragraph{Example 3:} Show that  $a \cdot b + b \cdot c = (a + c) \cdot b$ ~\\~\\
                \begin{tabular}{l l}
                    $a \cdot b + b \cdot c$ \\
                    $= b \cdot a + b \cdot c$ & commutative \\
                    $= b \cdot (a + c)$ & distributive \\
                    $= (a + c) \cdot b$ & commutative
                \end{tabular}

            \paragraph{Example 4:} Show that if  $a' + b = 1$ then $a \cdot b' = 0$. ~\\~\\
                \begin{tabular}{l l}
                    $a' + b$ \\
                    $= (a')' \cdot b'$ & double negative \\
                    $= (a' + b)'$ & DeMorgan \\
                    $= (1)'$ & Since $a' + b = 1$ \\
                    $= 0$ & Complements
                \end{tabular}
        \end{intro}

    \newpage

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Simplify the following equations using the Boolean Algebra properties.

            \begin{enumerate}
                \item[a.]   $ab + ab'$              \vspace{2cm}
                \item[b.]   $cd \cdot c'd'$         \vspace{2cm}
                \item[c.]   $e \cdot (e + f)$       \vspace{2cm}
                \item[d.]   $g \cdot (h + i)$       \vspace{2cm}
                \item[e.]   $j + (k \cdot l)$       \vspace{2cm}
                \item[f.]   $m + (no + no')$       \vspace{2cm}
            \end{enumerate}
        \end{questionNOGRADE}

    \newpage

        \begin{intro}{Example}
            Using the Boolean Algebra properties, transform the left-hand side
            of each equation to the right-hand side.

            Show that $ba + ba' = b$ ~\\ ~\\
            \begin{tabular}{l l l | l}
                & Property & & Step \\
                1. & Distributive & $a \cdot (b+c) = (ab) + (ac)$ & $ba + ba'$ $\to$ $b \cdot (a + a')$ \\
                2. & Negation & $a + a' = 1$ & $b \cdot (a + a')$ $\to$ $b \cdot (1)$ \\
                3. & Identity & $a \cdot 1 = a$ & $b \cdot (1)$ $\to$ $b \cdot 1 = b$
            \end{tabular}
            
            ~\\ Therefore $ba + ba' = b$
        \end{intro}

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Using the Boolean Algebra properties, transform the left-hand side
            of each equation to the right-hand side.

            \begin{enumerate}
                \item[c.]   $x'yz + x'y'z + xyz' + xy'z' = xz' + x'z$ \\
                        (Group up two terms at a time: $xyz' + xy'z'$ and $x'yz + x'y'z$.)
                    \solution{
                        \\ Associative: $= (xyz' + xy'z') + (x'yz + x'y'z)$
                        \\ Distributive: $= xz'(y + y') + x'z(y + y')$
                        \\ Negation: $= xz'(1) + x'z(1)$
                        \\ Identity: $= xz' + x'z$
                    }{ \vspace{4cm} }

                \item[d.]   $xyz + xyz' + x'yz + x'yz' = y$
                    \solution{
                        \\ Associative: $= (xyz + xyz') + (x'yz + x'yz')$
                        \\ Distributive: $= xy(z + z') + x'y(z + z')$
                        \\ Negation: $= xy(1) + x'y(1)$
                        \\ Identity: $= xy + x'y$
                        \\ Distributive: $= y(x + x')$
                        \\ Negation: $= y(1)$
                        \\ Identity: $= y$
                    }{ \vspace{4cm} }
            \end{enumerate}
        \end{questionNOGRADE}



    \newpage
    \subsection*{Logic Circuits}

    \begin{intro}{\ }
        We are going to be using logic gates as one way to represent our
        Boolean Algebra expressions graphically. The gates that we will be using are:

        \begin{center}
            \begin{tabular}{c c c}
                AND $\cdot$ & OR $+$ & NOT $'$ \\
                \includegraphics[height=4cm]{images/3-5-andgate.png} &
                \includegraphics[height=4cm]{images/3-5-orgate.png} &
                \includegraphics[height=4cm]{images/3-5-notgate.png}
                \\ \\
                \begin{tabular}{ c c | c }
                    $a$ & $b$ & $a \cdot b$ \\ \hline
                    0 & 0 & 0 \\
                    0 & 1 & 0 \\
                    1 & 0 & 0 \\
                    1 & 1 & 1
                \end{tabular}
                &

                \begin{tabular}{ c c | c }
                    $a$ & $b$ & $a + b$ \\ \hline
                    0 & 0 & 0 \\
                    0 & 1 & 1 \\
                    1 & 0 & 1 \\
                    1 & 1 & 1
                \end{tabular}
                &

                \begin{tabular}{ c | c }
                    $a$ & $a'$ \\ \hline
                    0 & 1 \\
                    1 & 0
                \end{tabular}
            \end{tabular}

        \end{center}

            ~\\
            Additionally, we can connect gates together in order to build an expression.
            For example:

        \begin{center}
            \begin{tabular}{c c c}
                $(a+b)'$ &
                $ ab + b' $ &
                $(a+b) \cdot (b' + c)$
                \\
                \includegraphics[height=6cm]{images/3-5-gate1.png} &
                \includegraphics[height=6cm]{images/3-5-gate2.png} &
                \includegraphics[height=6cm]{images/3-5-gate3.png}
            \end{tabular}
        \end{center}

    \end{intro}

    \newpage

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Write out the Boolean expression that describes each diagram:

            \begin{center}
                \begin{tabular}{c | c | c}
                    \includegraphics[height=8cm]{images/3-5-gate4.png} &
                    \includegraphics[height=8cm]{images/3-5-gate5.png} &
                    \includegraphics[height=8cm]{images/3-5-gate6.png}
                    \\ & &
                    \\ & &
                    \\ & &
                    \\  \solution{ $a + b'$ }{}
                    &   \solution{ $(a \cdot b) + b'$ }{}
                    &   \solution{ $ (a+b) \cdot (b'+c) $ }{}
                \end{tabular}
            \end{center}
        \end{questionNOGRADE}

    \newpage

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Draw a circuit diagram for the following Boolean expressions:

            \begin{tabular}{p{4cm} | p{4cm} | p{4cm}}
                a. $a + b'$ &
                b. $a' \cdot b'$ &
                c. $a + (b \cdot c)$
            \end{tabular}
        \end{questionNOGRADE}






\input{../BASE-4-FOOT}




