#include <string>
#include <iostream>
using namespace std;

void Output( int n )
{
    cout << n << "\t";
}

void Output( int n, int m )
{
    cout << n << ", " << m << "\t";
}


bool IsAnagram( string text )
{
    int a = 0;
    int z = text.size() - 1;

    while ( a < z )
    {
        if ( text[a] != text[z] )
        {
            return false;
        }
        a++;
        z--;
    }
    return true;
}


void DrawSquare( int width )
{
    for ( int y = 0; y < width; y++ )
    {
        for ( int x = 0; x < width; x++ )
        {
            cout << "*";
        }
        cout << "\n";
    }
}

int Find()

int main()
{
    string text = "boat";
    cout << text << "\t" << IsAnagram( text ) << "\n\n";

    text = "racecar";
    cout << text << "\t" << IsAnagram( text ) << "\n\n";

    text = "abba";
    cout << text << "\t" << IsAnagram( text ) << "\n\n";

    DrawSquare( 10 );
    
    for ( int i = 0; i < 5; i++ )
    {
        for ( int j = i; j < 5; j++ )
        {
            Output( i * j );
        }
    }

    cout << endl << endl;
    
    int i = 0;
    int j = 20;

    while ( i < j )
    {
        i++;
        j--;
        Output( i, j );
    }

    cout << endl << endl;
    
    return 0;
}
