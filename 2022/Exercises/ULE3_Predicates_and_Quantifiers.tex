% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Predicates and Quantifiers}
\newcommand{\laKey}         {U1E3}

\input{BASE-HEAD}


% -------------------------------------------------------------------- %

\begin{intro}{Predicates\\}
    In mathematical logic, a \textbf{predicate} is commonly understood to be a Boolean-valued function.
    \footnote{From https://en.wikipedia.org/wiki/Predicate\_(mathematical\_logic)}

    We write a predicate as a function, such as $P(x)$, for example:

    \begin{center}
        $P(x)$ is the predicate, ``x is less than 2".
    \end{center}

    Once some value is plugged in for $x$, the result is a proposition -
    something either unambiguously \textbf{true} or \textbf{false},
    but until we have some input for $x$, we don't know whether it
    is true or false.

    \begin{center}
        $P(0)$ = true \tab $P(2)$ = false \tab $P(10)$ = false
    \end{center}

    Additionally, predicates can also be combined with the logical
    operators AND $\land$ OR $\lor$ and NOT $\neg$, as well as building
    implications with predicates.
\end{intro}

\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    For the following predicates given, plug in \textbf{2, 23, -5, and 15}
    as inputs and write out whether the result is true or false.

    \begin{enumerate}
        \item[(a)] $P(x)$ is the predicate ``$x > 15$''
                    ~\\ \tab $P(2)=$    \vspace{0.5cm}
                    ~\\ \tab $P(23)=$   \vspace{0.5cm}
                    ~\\ \tab $P(-5)=$   \vspace{0.5cm}
                    ~\\ \tab $P(15)=$   \vspace{0.5cm}
        \item[(b)] $Q(x)$ is the predicate ``$x \leq 15$''
                    ~\\ \tab $Q(2)=$    \vspace{0.5cm}
                    ~\\ \tab $Q(23)=$   \vspace{0.5cm}
                    ~\\ \tab $Q(-5)=$   \vspace{0.5cm}
                    ~\\ \tab $Q(15)=$   \vspace{0.5cm}
        \item[(c)] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$''
                    ~\\ \tab $R(2)=$    \vspace{0.5cm}
                    ~\\ \tab $R(23)=$   \vspace{0.5cm}
                    ~\\ \tab $R(-5)=$   \vspace{0.5cm}
                    ~\\ \tab $R(15)=$   \vspace{0.5cm}
    \end{enumerate}
\end{questionNOGRADE}


% -------------------------------------------------------------------- %

\newpage
\begin{intro}{Domain}
    When we're working with predicates, we will also define the domain.

    The \textbf{domain} is the set of all possible inputs for our predicate.
    In other words, $x$ must be chosen from the domain.
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    For the following predicates and domains given, specify
    whether the predicate is true for \textbf{all members of the domain},
    \textbf{some members of the domain}, or \textbf{no members of the domain.}

    \begin{enumerate}
        \item[(a)]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.
                    ~\\
                    \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\

        \item[(b)] $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.
                    ~\\
                    \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\

        \item[(c)] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.
                    ~\\
                    \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\

        \item[(d)] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.
                    ~\\
                    \Square \ True for all \tab \Square \ True for some \tab \Square \ True for none \\
    \end{enumerate}
\end{questionNOGRADE}

% -------------------------------------------------------------------- %

\newpage
\begin{intro}{Quantifiers}
    Symbolically, we can specify that the input of our predicate, $x$,
    belongs in some domain set $D$ with the notation: $x \in D$.
    This is read as, ``$x$ exists in the domain $D$."

    Additionally, we can also specify whether a predicate is true
    \textbf{for all inputs $x$ from the domain $D$} using the ``for all"
    symbol $\forall$, or we can specify that the predicate is true
    \textbf{for \textit{some} inputs $x$ from the domain $D$} using
    the ``there exists" symbol $\exists$.{}

    \paragraph{Example:} Rewrite the predicate symbolically.
        $P(x)$ is ``$x > 15$", the domain D is \{16, 17, 18\}.
        Here we can see that all inputs from the domain will
        result in the predicate evaluating to true, so we can write:

        \begin{center}
            $ \forall x \in D, P(x) $ (``For all x in D, x is greater than 15.")
        \end{center}

    \begin{itemize}
        \item The symbol $\in$ (``in") indicates membership in a set.
        \item The symbol $\forall$ (``for all") means "for all", or "every".
        \item The symbol $\exists$ (``there exists") means "there is (at least one)", or "there exists (at least one)".
        \item The symbols $\forall$ and $\exists$ are called \textbf{quantifiers}. When used
            with predicates, the statement is called a \textbf{quantified predicate}.
    \end{itemize}
\end{intro}

\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    For the following predicates, rewrite the sentence symbolically,
    as in the example above.
    Use either $\forall$ or $\exists$, based on whether the
    predicate is true for the domain given.

    \paragraph{Hint:}
        If a predicate P(x) is false for all elements in the domain, you
        can phrase it as: ``$\forall x \in D, \neg P(x)$".

    \begin{enumerate}
        \item[(a)] $P(x)$ is the predicate ``$x > 15$", the domain $D$ is \{10, 12, 14, 16, 18\}.
                    ~\\ \vspace{2cm}
        \item[(b)] $Q(x)$ is the predicate ``$x \leq 15$", the domain $E$ is \{0, 1, 2, 3\}.
                    ~\\ \vspace{2cm}
        \item[(c)] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain $F$ is \{0, 1, 2\}.
                    ~\\ \vspace{2cm}
        \item[(d)] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain $G$ is \{2, 3, 4\}.
                    ~\\ \vspace{2cm}
    \end{enumerate}
\end{questionNOGRADE}

% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Translate the following English statement into a quantified statement.
    Make sure to define your \textbf{predicate} (state that $P(x)$ is the predicate ``\fitb''){}
    and specify whether the quantified statement is \textbf{true} or \textbf{false}.

    ~\\ \textbf{Given the domain $D = \{ 1, 2, 3, 4, 5 \}$}

    \begin{enumerate}
      \item[(a)]    All inputs from $D$ are positive numbers.
        \begin{itemize}
          \item   \textbf{Predicate:}
          \item   \textbf{Quantified statement:}
          \item   \textbf{True or false?}
        \end{itemize}
        
      \item[(b)]    There exists some input from $D$ that is a negative number.
        \begin{itemize}
          \item   \textbf{Predicate:}
          \item   \textbf{Quantified statement:}
          \item   \textbf{True or false?}
        \end{itemize}
      
      \item[(c)]    All inputs from the domain $D$ are greater than or equal to 2.
        \begin{itemize}
          \item   \textbf{Predicate:}
          \item   \textbf{Quantified statement:}
          \item   \textbf{True or false?}
        \end{itemize}
        
      \item[(d)]    There exists (at least one) input $k$ from $D$ such that its square $k^{2}$ is also in the set $D$.
        \begin{itemize}
          \item   \textbf{Predicate:}
          \item   \textbf{Quantified statement:}
          \item   \textbf{True or false?}
        \end{itemize}
    \end{enumerate}

\end{questionNOGRADE}

% -------------------------------------------------------------------- %

\newpage
\begin{intro}{Negating quantifiers}
    When negating a quantified statement, any $\forall$ quantifiers get changed to
    $\exists$ and vice versa. Additionally, the predicate is also negated.
    
  \begin{center} \large
    \begin{tabular}{| c | c |} \hline
      \textbf{Original} & \textbf{Negation} \\ \hline
      $\forall x \in D, P(x)$ & $\exists x \in D, \neg P(x)$ \\ \hline
      $\exists x \in D, P(x)$ & $\forall x \in D, \neg P(x)$ \\ \hline
    \end{tabular}
  \end{center}
  
  Additionally, note that:
  \begin{itemize}
    \item   The negation of $a = b$ is $a \neq b$.
    \item   The negation of $a > b$ is $a \leq b$.
    \item   The negation of $a \geq b$ is $a < b$.
    \item   The negation of $a < b$ is $a \geq b$.
    \item   The negation of $a \leq b$ is $a > b$.
    \item   The negation of $a \in D$ is $a \not\in D$.
    \item   The negation of $a \land b$ is $\neg a \lor \neg b$.
    \item   The negation of $a \lor b$ is $\neg a \land \neg b$.
  \end{itemize}
\end{intro}

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Write the negation of each of these statements. Simplify as much as possible.
    \begin{enumerate}
        \item[(a)] $\forall x \in \mathbb{Z},$ \tab[0.2cm] $\exists y \in \mathbb{Z},$ \tab[0.2cm] $2x+y = 3$ 
                  \solution{ 
                  ~\\ $\equiv \neg ( \forall x \in \mathbb{Z})$, $\neg ( \exists y \in \mathbb{Z} )$, $\neg ( 2x+y = 3 )$
                  ~\\ $\equiv \exists x \in \mathbb{Z}, \forall y \in \mathbb{Z}, 2x+y \neq 3$.
                  }{}
        \item[(b)] $\exists x \in \mathbb{N},$ \tab[0.2cm] $\forall y \in \mathbb{N},$ \tab[0.2cm] $x \cdot y < x$
    \end{enumerate}
\end{questionNOGRADE}

\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Write the negation of the statement. Simplify as much as possible.
    \begin{center}
      $\exists x \in \mathbb{Z},$ \tab[0.2cm] $\exists y \in \mathbb{Z},$ \tab[0.2cm] $(x + y = 13) \land (x \cdot y = 36)$
    \end{center}
    
    \begin{hint}{Hint}
      It might help to create some propositional variables, like this:
      
      \begin{enumerate}
        \item   $p: x + y = 13$, \tab $q: x \cdot y = 36$,
        \item   $(x + y = 13) \land (x \cdot y = 36) \equiv p \land q$
        \item   $\neg ( p \land q ) \equiv \neg p \lor \neg q$
      \end{enumerate}
      
      Then you can expand again, negating each proposition:
      
      \begin{enumerate}
        \item   $\neg p \equiv \neg (x + y = 13) \equiv x + y \neq 13$
        \item   $\neg q \equiv \neg (x \cdot y = 36) \equiv x \cdot y \neq 36$
      \end{enumerate}
      
      So $\neg[ (x + y = 13) \land (x \cdot y = 36) ] \equiv (x + y \neq 13) \lor (x \cdot y \neq 36)$
    \end{hint}
\end{questionNOGRADE}

\newpage

\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Which elements of the set D = \{2, 4, 6, 8, 10, 12\} make the
    \textbf{negation} of each of these predicates true?
    \begin{enumerate}
        \item[(a)] $Q(n)$ is the predicate, ``$n > 10$".
                    \solution{
                    ~\\ Negation $\neg Q(n)$ is $n \leq 10$.
                    ~\\ 2, 4, 6, 8, and 10 make $\neg Q(n)$ true
                    (or $Q(n)$ false).
                    }{}
        \item[(b)] $R(n)$ is the predicate, ``$n$ is even".                 ~\\ \vspace{4cm}
        \item[(c)] $S(n)$ is the predicate, ``$n^{2} < 1$".                 ~\\ \vspace{4cm}
        \item[(d)] $T(n)$ is the predicate, ``$n-2$ is an element of $D$".  ~\\ \vspace{4cm}
    \end{enumerate}
\end{questionNOGRADE}

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{

\begin{enumerate}
  \item
    \begin{enumerate}
        \item[(a)] $P(x)$ is the predicate ``$x > 15$''
                    ~\\ \tab $P(2)=   2 > 15$, false
                    ~\\ \tab $P(23)=  23 > 15$, true
                    ~\\ \tab $P(-5)=  -5 > 15$, false
                    ~\\ \tab $P(15)=  15 > 15$, false
        \item[(b)] $Q(x)$ is the predicate ``$x \leq 15$''
                    ~\\ \tab $Q(2)=   2 \leq 15$, true
                    ~\\ \tab $Q(23)=  23 \leq 15$, false
                    ~\\ \tab $Q(-5)=  -5 \leq 15$, true
                    ~\\ \tab $Q(15)=  15 \leq 15$, true
        \item[(c)] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$''
                    ~\\ \tab $R(2)=   ( 2 > 5 ) \land ( 2 < 20 )$,  true
                    ~\\ \tab $R(23)=  ( 23 > 5 ) \land ( 23 < 20 )$,  false
                    ~\\ \tab $R(-5)=  ( -5 > 5 ) \land ( -5 < 20 )$,  false
                    ~\\ \tab $R(15)=  ( 15 > 5 ) \land ( 15 < 20 )$,  true
    \end{enumerate}

  \item
    \begin{enumerate}
        \item[(a)]   $P(x)$ is the predicate ``$x > 15$", the domain is \{10, 12, 14, 16, 18\}.     ~\\ \textbf{True for some}
        \item[(b)] $Q(x)$ is the predicate ``$x \leq 15$", the domain is \{0, 1, 2, 3\}.            ~\\ \textbf{True for all}
        \item[(c)] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain is \{0, 1, 2\}.  ~\\ \textbf{True for none}
        \item[(d)] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain is \{2, 3, 4\}.       ~\\ \textbf{True for all}
    \end{enumerate}

  \item
    \begin{enumerate}
        \item[(a)] $P(x)$ is the predicate ``$x > 15$", the domain $D$ is \{10, 12, 14, 16, 18\}.       ~\\ $ \exists x \in D, P(x)$
        \item[(b)] $Q(x)$ is the predicate ``$x \leq 15$", the domain $E$ is \{0, 1, 2, 3\}.            ~\\ $ \forall x \in E, Q(x)$
        \item[(c)] $R(x)$ is the predicate ``$(x > 5) \land (x < 20)$", the domain $F$ is \{0, 1, 2\}.  ~\\ $ \forall x \in F, \neg R(x) $
        \item[(d)] $S(x)$ is the predicate ``$(x > 1) \land (x < 5)$", domain $G$ is \{2, 3, 4\}.       ~\\  $ \forall x \in G, S(x)$
    \end{enumerate}

  \newpage
  \item 
    \begin{enumerate}
      \item[(a)]    All inputs from $D$ are positive numbers. ~\\
        \begin{tabular}{l l}
              \textbf{Predicate:}             & $P(x)$ is the predicate ``$x$ is a positive number''.
          \\  \textbf{Quantified statement:}  & $\forall x \in D, P(x)$
          \\  \textbf{True or false?}         & true, all numbers are positive.
        \end{tabular}
        
      \item[(b)]    There exists some input from $D$ that is a negative number. ~\\
        \begin{tabular}{l l}
              \textbf{Predicate:}             & $N(x)$ is the predicate ``$x$ is a negative number''.
          \\  \textbf{Quantified statement:}  & $\exists x \in D, N(x)$
          \\  \textbf{True or false?}         & false, no numbers in the set are negative.
        \end{tabular}
      
      \item[(c)]    All inputs from the domain $D$ are greater than or equal to 2. ~\\
        \begin{tabular}{l l}
              \textbf{Predicate:}             & $G(x)$ is $x \geq 2$
          \\  \textbf{Quantified statement:}  & $\forall x \in D, G(x)$
          \\  \textbf{True or false?}         & false, $1 \not\geq 2$
        \end{tabular}
        
      \item[(d)]    There exists (at least one) input $k$ from $D$ such that its square $k^{2}$ is also in the set $D$. ~\\
        \begin{tabular}{l l}
              \textbf{Predicate:}             & $S(k)$ is the predicate $k^{2} \in D$
          \\  \textbf{Quantified statement:}  & $\exists k \in D, S(k)$
          \\  \textbf{True or false?}         & true, $2^{2} = 4$, and both 2 and 4 are in $D$.
        \end{tabular}
    \end{enumerate}
    
  \item
    \begin{enumerate}
        \item[(a)] $\forall x \in \mathbb{Z},$ \tab[0.2cm] $\exists y \in \mathbb{Z},$ \tab[0.2cm] $2x+y = 3$ 
                  \solution{ 
                  ~\\ $\equiv \neg ( \forall x \in \mathbb{Z})$, $\neg ( \exists y \in \mathbb{Z} )$, $\neg ( 2x+y = 3 )$
                  ~\\ $\equiv \exists x \in \mathbb{Z}, \forall y \in \mathbb{Z}, 2x+y \neq 3$.
                  }{}
        \item[(b)] $\exists x \in \mathbb{N},$ \tab[0.2cm] $\forall y \in \mathbb{N},$ \tab[0.2cm] $x \cdot y < x$
                  ~\\ $\equiv \neg( \exists x \in \mathbb{N} )$, 
                      $\neg(\forall y \in \mathbb{N})$, 
                      $\neg(x \cdot y < x)$
                  ~\\ $\equiv \forall x \in \mathbb{N}, \exists y \in \mathbb{N}, x \cdot y \geq x$
    \end{enumerate}
    
  \item   $\exists x \in \mathbb{Z},$ \tab[0.2cm] $\exists y \in \mathbb{Z},$ \tab[0.2cm] $(x + y = 13) \land (x \cdot y = 36)$
          ~\\   $\equiv \neg( \exists x \in \mathbb{Z} )$
                $\neg( \exists y \in \mathbb{Z} )$
                $\neg( (x + y = 13) \land (x \cdot y = 36) )$
          ~\\   $\equiv \forall x \in \mathbb{Z}, \forall y \in \mathbb{Z},$
                $\neg( (x + y = 13) \land (x \cdot y = 36) )$
          ~\\   $\equiv \forall x \in \mathbb{Z}, \forall y \in \mathbb{Z},$
                $\neg(x + y = 13) \lor \neg(x \cdot y = 36)$
          ~\\   $\equiv \forall x \in \mathbb{Z}, \forall y \in \mathbb{Z},$
                $(x + y \neq 13) \lor (x \cdot y \neq 36)$
  
  \newpage
  \item   $D = \{2, 4, 6, 8, 10, 12\}$
    \begin{enumerate}
        \item[(a)] $Q(n)$ is the predicate, ``$n > 10$".
                    ~\\ Negation $\neg Q(n)$ is $n \leq 10$.
                    ~\\ 2, 4, 6, 8, and 10 make $\neg Q(n)$ true
                    (or $Q(n)$ false).
        \item[(b)] $R(n)$ is the predicate, ``$n$ is even".
                    ~\\ Negation $\neg R(n)$ is ``$n$ is not even''.
                    ~\\ None of the elements of $D$ make the negation true/make $R(n)$ false.
        \item[(c)] $S(n)$ is the predicate, ``$n^{2} < 1$".
                    ~\\ Negation $\neg S(n)$ is ``$n^{2} \geq 1$''.
                    ~\\ All elements of $D$ make the negation true/make $S(n)$ false.
        \item[(d)] $T(n)$ is the predicate, ``$n-2$ is an element of $D$".
                    ~\\ Negation $\neg T(n)$ is ``$n-2$ is NOT an element of $D$''.
                    ~\\ $n-2 \not\in D$ is true for the element 2.
    \end{enumerate}
\end{enumerate}

}{}
\input{BASE-FOOT}


