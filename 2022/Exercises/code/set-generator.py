import random

A=[]
B=[]
C=[]
D=[]

for i in range( 4 ):
  A.append( random.randint( 1, 9 ) )
  B.append( random.randint( 1, 9 ) )
  C.append( random.randint( 1, 9 ) )
  D.append( random.randint( 1, 9 ) )

print( "A = ", A )
print( "B = ", B )
print( "C = ", C )
print( "D = ", D )

print( "\nUNIONS" )

AuB = list(set(A + B))
AuC = list(set(A + C))
AuD = list(set(A + D))
BuC = list(set(B + C))
BuD = list(set(B + D))
CuD = list(set(C + D))

print( "AuB = ", AuB )
print( "AuC = ", AuC )
print( "AuD = ", AuD )
print( "BuC = ", BuC )
print( "BuD = ", BuD )
print( "CuD = ", BuD )


def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3
    
print( "\nINTERSECTIONS" )
AnB = intersection(A, B)
AnC = intersection(A, C)
AnD = intersection(A, D)
BnC = intersection(B, C)
BnD = intersection(B, D)
CnD = intersection(C, D)

print( "AnB = ", AnB )
print( "AnC = ", AnB )
print( "AnD = ", AnB )
print( "BnC = ", BnC )
print( "BnD = ", BnD )
print( "CnD = ", CnD )

print( "\nDIFFERENCES" )
AmB = set(A) - set(B)
BmA = set(B) - set(A)
AmC = set(A) - set(C)
CmA = set(C) - set(A)
AmD = set(A) - set(D)
DmA = set(D) - set(A)

BmC = set(B) - set(C)
CmB = set(C) - set(B)
BmD = set(B) - set(D)
DmB = set(D) - set(B)

CmD = set(C) - set(D)
DmC = set(D) - set(C)

print( "A-B = ", AmB )
print( "B-A = ", BmA )
print( "A-C = ", AmC )
print( "C-A = ", CmA )
print( "A-D = ", AmD )
print( "D-A = ", DmA )

print( "B-C = ", BmC )
print( "C-B = ", CmB )
print( "B-D = ", BmD )
print( "D-B = ", DmB )

print( "C-D = ", CmD )
print( "D-C = ", DmC )

print( "\nCOMPOUND" )
