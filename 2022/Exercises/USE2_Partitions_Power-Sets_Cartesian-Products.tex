% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Set Operations}
\newcommand{\laKey}         {USE2}

\input{BASE-HEAD}

% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\begin{intro}{Partitions}
    The Partition of a set, usually denoted by $S$, is a \textbf{set of subsets} that,
    when combined together, form the \textbf{original set}.

    \paragraph{Definition:}
    For a set $A$, a partition of $A$ is some set $S = \{S_{1} , S_{2} , S_{3} , ...\}$ of subsets of $A$, such that:

    \begin{enumerate}
        \item   For all $i$, $S_{i} \neq \emptyset$ - that is, each \textit{part} is non-empty.
        \item   For all $i$ and $j$, if $S_{i} \neq S_{j}$, then $S_{i} \cap S_{j} = \emptyset$ - that is, different \textit{parts} have \underline{nothing} in common.
        \item   $S_{1} \cup S_{2} \cup S_{3} \cup ... = A$ - that is, every element in $A$ is contained in some \textit{part}.
    \end{enumerate}

    \subparagraph{Clarification:}
        The partition is known as $S$. Each element of $S$, such as $S_{i}$, is known as a \textbf{part}. A Part is a set as well.
        No parts are empty sets, and all parts must have some elements that come from $A$. An element of $A$ cannot be repeated across
        multiple Parts, and all elements of $A$ must be represented in the entire partition $S$.
\end{intro}
\newpage
\begin{intro}{Partition Example}
        Let's say we have a set, $A = \{ 1, 2, 3, 4 \}$. We could form multiple partitions, such as:

        \begin{itemize}
            \item   Partition 1:    $ \{ \{1\}, \{2\}, \{3\}, \{4\} \} $
            \item   Partition 2:    $ \{ \{1, 2\}, \{3, 4\} \} $
            \item   Partition 3:    $ \{ \{1, 2, 3\}, \{4\} \} $
            \item   Partition 4:    $ \{ \{1, 2, 3, 4\} \} $
        \end{itemize}
\end{intro}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Write out all possible partitions of $A = \{1, 2\}$. There
    should be 2. Note that the order of the elements of the set does not matter.

    \begin{enumerate}
        \item
        \item           \vspace{0.75cm}
    \end{enumerate}     \vspace{0.75cm}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Write out all possible partitions of $B = \{1, 2, 3\}$. There
    should be 5.
    ~\\~\\
    \begin{tabular}{p{6cm} p{6cm}}
        1.  \vspace{0.7cm}
        \\ & 2. \vspace{0.7cm}
        \\
        3.   \vspace{0.7cm}
        \\ & 4. \vspace{0.7cm}
        \\
        5.
    \end{tabular}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  For the set $A = \{ 1, 2, 3, 4, 5, 6 \}$, build partitions that meet the following criteria:
  
  \begin{enumerate}
    \item[(a)]    Find a partition where each part has the same size.     \vspace{4cm}
    \item[(b)]    Find a partition where no two parts have the same size. \vspace{4cm}
    \item[(c)]    Find a partition that has as many parts as possible.    \vspace{4cm}
    \item[(d)]    Find the partition that has as few parts as possible.   \vspace{4cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Power Sets}
    The Power Set of $A$ is defined as $\wp(A) = \{ S : S \subseteq A \}$.
    In other words, the Power Set is the \textbf{set of all possible subsets
    that you could build from $A$, INCLUDING the empty set.}

    \subparagraph{Example 1:} Find the Power Set of $\{ A \}$.
        \begin{center}
            $\wp( \{A\} ) = \{ \emptyset, \{A\} \}$
        \end{center}
    \subparagraph{Example 2:} Find the Power Set of $\{ A, B \}$.
        \begin{center}
            $\wp( \{A, B\} ) = \{ \emptyset, \{A\},\{B\}, \{A, B\} \}$
        \end{center}
    \subparagraph{Example 3:} Find the Power Set of $\{ A, B, C \}$.
        \begin{center}
            $\wp( \{A, B, C\} ) = \{ \emptyset, \{A\}, \{B\}, \{C\}, \{A, B\}, \{B, C\}, \{A, C\}, \{A, B, C\} \}$
        \end{center}
    \subparagraph{Example 4:} Find the Power Set of $\{ A, B, C, D \}$.

        $\wp( \{A, B, C, D\} ) = \{$     \\ \tab
            $\emptyset$, \\ \tab
            $ \{A\}, \{B\}, \{C\}, \{D\}, $ \\ \tab
            $ \{A, B\}, \{A, C\}, \{A, D\}, \{B, C\}, \{B, D\}, \{C, D\},$ \\ \tab
            $ \{A, B, C\}, \{A, B, D\}, \{A, C, D\}, \{B, C, D\}, $ \\ \tab
            $ \{A, B, C, D\}$
        \\ $ \} $
        ~\\ \footnotesize (Phew!)
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Find the Power Set for the following:
    \begin{enumerate}
        \item[a.]   $\wp(\{1, 2\}) = $  \vspace{1.5cm}
        \item[b.]   $\wp(\{3, 4\}) = $
    \end{enumerate}
\end{questionNOGRADE}
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Find the Power Set for $\wp(\{1, 2, 3\})$.
    \vspace{4cm}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Cartesian Products}
    We can compute the Cartesian Product of two sets, such as $A$ and $B$.
    The result will be a set of \textbf{ordered pairs}, such as $(a, b)$, combining the
    elements of $A$ and $B$ together.

    \subparagraph{Example:}
        For $A = \{1, 2\}$ and $B = \{4, 5, 6\}$, find $A \times B$.

        \begin{center}
            \begin{tabular}{ c | c | c | c }
                &   $B_{1} = 4$ & $B_{2} = 5$ & $B_{3} = 6$ \\ \hline
                $A_{1} = 1$ & $(1, 4)$ & $(1, 5)$ & $(1, 6)$
                \\
                $A_{2} = 2$ & $(2, 4)$ & $(2, 5)$ & $(2, 6)$
            \end{tabular}
        \end{center}

        So the final result is:

        \begin{center}
            $A \times B = \{ (1,4), (1,5), (1,6), (2,4), (2,5), (2,6) \}$
        \end{center}
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Find the Cartesian Product of $C \times S$ given $C = \{ red, green \}$
    and $S = \{ shirt \}$

        \begin{center}
            \begin{tabular}{ c | p{5cm} | p{5cm} }
                &   $C_{1} = red$ & $C_{2} = green$ \\ \hline
                & & \\
                $S_{1} = shirt$ &  &
                \\ & &
            \end{tabular}
        \end{center}
    \vspace{1cm}
    $ C \times S = $
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Given the sets find the following Cartesian Products.

    \begin{center}
        $N = \{1, 2\}$ \tab $L = \{a, b\}$
    \end{center}

    \large
    \begin{itemize}
        \item[(a)]  $N \times L$
          \begin{center}
              \begin{tabular}{ c | p{5cm} | p{5cm} }
                  &   $N_{1} = 1$ & $N_{2} = 2$ \\ \hline
                  $L_{1} = a$ &  &    \\      & & \\ \hline
                  $L_{2} = b$ &  &    \\      & & \\ 
              \end{tabular}
          \end{center}
          ~\\
          $N \times L = $
          \vspace{2cm}
                    
        \item[(b)]  $L \times N$
          \begin{center}
              \begin{tabular}{ c | p{5cm} | p{5cm} }
                  &   $L_{1} = a$ & $L_{2} = b$ \\ \hline
                  $N_{1} = 1$ &  &    \\      & & \\ \hline
                  $N_{2} = 2$ &  &    \\      & & \\ 
              \end{tabular}
          \end{center}
          ~\\
          $L \times N = $
          \vspace{1cm}
    \end{itemize}
    \normalsize
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Given the sets find the following Cartesian Products.

    \begin{center}
        $C = \{x, y, z\}$ \tab $Z = \{5, 10\}$
    \end{center}

    \large
    \begin{itemize}
        \item[a.]   $C \times Z$
          \begin{center}
              \begin{tabular}{ c | p{3cm} | p{3cm} | p{3cm} }
                  &   $C_{1} = x$ & $C_{2} = y$ & $C_{3} = z$ \\ \hline
                  $Z_{1} = 5$ &  &    \\  & & \\ \hline
                  $Z_{2} = 10$ &  &    \\ & & \\ 
              \end{tabular}
          \end{center}
          ~\\
          $C \times Z = $
          \vspace{1.5cm}
                    
        \item[b.]   $Z \times C$
          \begin{center}
              \begin{tabular}{ c | p{5cm} | p{5cm} }
                  &   $Z_{1} = 5$ & $Z_{2} = 10$ \\ \hline
                  $C_{1} = x$ &  &    \\  & & \\ \hline
                  $C_{2} = y$ &  &    \\  & & \\ \hline
                  $C_{2} = z$ &  &    \\  & & \\ 
              \end{tabular}
          \end{center}
          ~\\
          $Z \times C = $
          \vspace{1cm}
    \end{itemize}
    \normalsize
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
\newpage
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
  Calculate the Cartesian Products and write out the result as a set of coordinate pairs.
  
  \begin{center}
      $A = \{2, 4\}$ \tab $B = \{1, 3\}$ \tab $C = \{3, 4, 5, 6\}$
  \end{center}
  
  \begin{enumerate}
    \item[(a)]  $A \times B$                    \vspace{3cm}
    \item[(b)]  $A \times C$                    \vspace{3cm}
    \item[(c)]  $B \times C$                    \vspace{3cm}
    \item[(d)]  $A^{2}$ (Hint: $A \times A$)    \vspace{3cm}
  \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\begin{intro}{Common pitfalls}
    Students frequently mix up Power Sets, Partitions, and Cartesian Products.
    Make sure that you understand the difference between each:

    \begin{itemize}
        \item   Partition of $X$:
                A \underline{set of sets} (parts), where each set contains some elements of the original set $X$. No sets can contain the same items, and no set can be empty. All elements of $X$ must be represented in the partition.
        \item   Power Set of $X$:
                A \underline{set of sets} that gives you all possible subsets of the original set $X$, \textit{including} the empty set.
        \item   Cartesian Product of $X$ and $Y$:
                A \underline{set of ordered pairs} that combines all elements of $X$ with all elements of $Y$.
    \end{itemize}
\end{intro}
% -------------------------------------------------------------------- %
\stepcounter{question}
\begin{questionNOGRADE}{\thequestion}
    Solve the following, given the set
  
    \begin{center}
      $A = \{ 1, 2 \}$
    \end{center}
    
    \begin{enumerate}
      \item[(a)]    List a partition of $A$   \vspace{2.5cm}
      \item[(b)]    $\wp(A) = $               \vspace{2.5cm}
      \item[(c)]    $A \times A = $           \vspace{2.5cm}
    \end{enumerate}
\end{questionNOGRADE}
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}
\solution{
  \begin{enumerate}
    \item   $A = \{1, 2\}$
      \begin{enumerate}
        \item $\{ \{1, 2 \} \}$
        \item $\{ \{1\}, \{2\} \}$
      \end{enumerate}
      
    \item   $B = \{1, 2, 3\}$
      \begin{enumerate}
        \item   $\{ \{1, 2, 3 \} \}$
        \item   $\{ \{1\}, \{2\}, \{3\} \}$
        \item   $\{ \{1, 2\}, \{3\} \}$
        \item   $\{ \{1\}, \{2, 3\} \}$
        \item   $\{ \{1, 3\}, \{2\} \}$
      \end{enumerate}
      
    \item   $A = \{1, 2, 3, 4, 5, 6\}$; Multiple answers
      \begin{enumerate}
        \item   $\{ \{1, 2\}, \{3, 4\}, \{5, 6\} \}$ is one example
        \item   $\{ \{1\}, \{2, 3\}, \{4, 5, 6\} \}$
        \item   $\{ \{1\}, \{2\}, \{3\}, \{4\}, \{5\}, \{6\} \}$
        \item   $\{ \{1, 2, 3, 4, 5, 6\} \}$
      \end{enumerate}
      
    \item  
      \begin{enumerate}
        \item   $\wp(\{1, 2\}) = \{ \emptyset, \{1\}, \{2\}, \{1, 2\} \}$
        \item   $\wp(\{3, 4\}) = \{ \emptyset, \{3\}, \{4\}, \{3, 4\} \}$
      \end{enumerate}
      
    \item   $\wp(\{1, 2, 3\}) = \{ \emptyset, \{A\}, \{B\}, \{C\}, \{A, B\}, \{B, C\}, \{A, C\}, \{A, B, C\} \}$
    
    \item   $C = \{ red, green \}$ and $S = \{ shirt \}$ ~\\
            \begin{tabular}{ c | p{5cm} | p{5cm} }
                                & $C_{1} = red$ & $C_{2} = green$   \\ \hline
                $S_{1} = shirt$ & (red, shirt)  & (green, shirt)    \\ 
            \end{tabular}
            ~\\
            $C \times S = \{ (red, shirt), (green, shirt) \}$
     
    \newpage
    \item   $N = \{1, 2\}$ \tab $L = \{a, b\}$ 
      \begin{itemize}
          \item[(a)]  $N \times L$ ~\\
                \begin{tabular}{ c | p{5cm} | p{5cm} }
                                & $N_{1} = 1$       & $N_{2} = 2$     \\ \hline
                    $L_{1} = a$ & (1, a)            & (2, a)          \\      
                    $L_{2} = b$ & (1, b)            & (2, b)          \\      
                \end{tabular}
            ~\\
            $N \times L = \{ (1, a), (2, a), (1, b), (2, b) \}$ 
            ~\\
                      
          \item[(b)]  $L \times N$ ~\\
                \begin{tabular}{ c | p{5cm} | p{5cm} }
                                  & $L_{1} = a$       & $L_{2} = b$   \\ \hline
                    $N_{1} = 1$   & $(a, 1)$           & $(b, 1)$      \\      
                    $N_{2} = 2$   & $(a, 2)$           & $(b, 2)$      \\      
                \end{tabular}
            ~\\
            $L \times N = \{ (a, 1), (b, 1), (a, 2), (b, 2) \}$ ~\\
            \vspace{1cm}
      \end{itemize}
      
    \item   $C = \{x, y, z\}$ \tab $Z = \{5, 10\}$
      \begin{itemize}
        \item[(a)]   $C \times Z$ ~\\
              \begin{tabular}{ c | p{3cm} | p{3cm} | p{3cm} }
                                & $C_{1} = x$     & $C_{2} = y$   & $C_{3} = z$ \\ \hline
                  $Z_{1} = 5$   & $(x, 5)$        & $(y, 5)$      & $(z, 5)$    \\ 
                  $Z_{2} = 10$  & $(x, 10)$       & $(y, 10)$     & $(z, 10)$   \\ 
              \end{tabular}
          ~\\
          $C \times Z = \{ (x, 5), (y, 5), (z, 5), (x, 10), (y, 10), (z, 10) \}$ ~\\
        
        \item[(b)]   $Z \times C$ ~\\
              \begin{tabular}{ c | p{5cm} | p{5cm} }
                              & $Z_{1} = 5$     & $Z_{2} = 10$      \\ \hline
                  $C_{1} = x$ & $(5, x)$        & $(10, x)$         \\ 
                  $C_{2} = y$ & $(5, y)$        & $(10, y)$         \\ 
                  $C_{2} = z$ & $(5, z)$        & $(10, z)$         \\ 
              \end{tabular}
      \end{itemize}
      ~\\
      $Z \times C = \{ (5, x), (10, x), (5, y), (10, y), (5, z), (10, z) \}$ ~\\
      
    \item   $A = \{2, 4\}$ \tab $B = \{1, 3\}$ \tab $C = \{3, 4, 5, 6\}$
      \begin{enumerate}
        \item[(a)]  $A \times B = \{ (2, 1), (2, 3), (4, 1), (4, 3) \}$                    
        \item[(b)]  $A \times C = \{ (2, 3), (2, 4), (2, 5), (2, 6), (4, 3), (4, 4), (4, 5), (4, 6) \}$                    
        \item[(c)]  $B \times C = \{ (1, 3), (1, 4), (1, 5), (1, 6), (3, 3), (3, 4), (3, 5), (3, 6) \}$                    
        \item[(d)]  $A^{2} = \{ (2, 2), (2, 4), (4, 4), (4, 2) \}$
      \end{enumerate}
      
    \newpage
    \item   $A = \{ 1, 2 \}$
      \begin{enumerate}
        \item[(a)]    List a partition of $A$ (multiple answer) $\{ \{1\}, \{2\} \}$   
        \item[(b)]    $\wp(A) = \{ \emptyset, \{1\}, \{2\}, \{1, 2\} \}$               
        \item[(c)]    $A \times A = \{ (1, 1), (1, 2), (2, 2), (2, 1) \} $           
      \end{enumerate}
  \end{enumerate}
}


%}{
%% KEY ------------------------------------ %

    %\begin{enumerate}
        %\item   $\{ \{ 1 \}, \{ 2 \} \}$ \\
                %$\{ \{ 1, 2, \} \}$
        %\item   $\{ \{1\}, \{2\}, \{3\} \}$  \\
                %$\{ \{1, 2\}, \{3\} \}$ \\
                %$\{ \{1, 3\}, \{2\} \}$ \\
                %$\{ \{2, 3\}, \{1\} \}$ \\
                %$\{ \{1, 2, 3\} \}$
            %\begin{itemize}
                %\item[a.]   asdf
                %\item[b.]   asdf
                %\item[c.]   asdf
            %\end{itemize}
    %\end{enumerate}

%}

\input{BASE-FOOT}

