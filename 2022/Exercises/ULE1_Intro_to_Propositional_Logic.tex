% --- ASSIGNMENT INFO --- %
\newcommand{\laClass}       {CS 210}
\newcommand{\laTopic}       {Introduction to Propositional Logic}
\newcommand{\laKey}         {U1E1}

\input{BASE-HEAD}

  \begin{intro}{Propositional Logic}
      A \textbf{proposition} is a statement which has truth value: it is either true (T) or false (F).
      \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}
      The statement doesn't have to necessarily be TRUE, it could also
      be FALSE, but it has to be unambiguously so. ~\\

      \begin{tabular}{ | l | l | l | }
          \hline
          \textbf{Statement} & \textbf{Is a proposition?} & \textbf{Truth value?}
          \\ \hline
          2 + 3 = 5 & Yes & True
          \\ \hline
          2 + 2 = 5 & Yes & False
          \\ \hline
          This class has 30 students & Yes & False\footnote{Probably :)}
          \\ \hline
          Is the movie good? & No &
          \\ \hline
          Go over there. & No &
          \\ \hline
          $n$ is even. & No (Don't know $n$!) &
          \\ \hline
      \end{tabular}
  \end{intro}

  \stepcounter{question}
  \begin{questionNOGRADE}{\thequestion}
      For the following, mark whether the statement is a \textbf{proposition} and,
      if it is, mark whether it is \textbf{true} or \textbf{false}.

      \begin{center}
       
          \begin{tabular}{l l | c | c}
              & \textbf{Statement} & \textbf{Proposition?} & \textbf{Truth value?} \\ \hline
              a. &    10 + 20 = 30          &                   &                   \\ & & \\ \hline                
              b. &    Is $4 < 5$?           &                   &                   \\ & & \\ \hline
              c. &    It is raining today.  &                   &                   \\ & & \\ \hline
              d. &    $x \cdot y$ is odd.   &                   &                   \\ & & \\ \hline
              e. &    $5 + 1$ is odd.       &                   &                   \\ & & \\ \hline
              f. &    Go to the store.      &                   &                   \\ & & \\ \hline
          \end{tabular}
          
      \end{center}
  \end{questionNOGRADE}


% -------------------------------------------------------------------- %

    \begin{intro}{Compound Propositions}
      We can also create a \textbf{compound proposition} using the logical operators
      for AND $\land$, OR $\lor$, and NOT $\neg$. When we're writing out
      a compound proposition, we will usually assign each proposition a
      \textbf{propositional variable}.

      ~\\ If we have the propositions
      $p$ is the proposition ``Paul is taking discrete math",
      $q$ is the proposition, ``Paul has a calculator",
      then we can build the compound propositions like:
      \begin{itemize}
          \item[] $p \land q$: Paul is taking discrete math and Paul has a calculator
          \item[] $p \lor q$: Paul is taking discrete math or Paul has a calculator
          \item[] $\neg p$: Paul is NOT taking discrete math
          \item[] $\neg q$: Paul does NOT have a calculator
      \end{itemize}

      The result of a compound proposition depends on the value of
      each proposition it is made up of:

      \begin{enumerate}
          \item A compound proposition $a \land b \land c$ is \textbf{only true}
              if all propositions are true; it will be \textbf{false} if
              one or more of the propositions is false.
          \item A compound proposition $a \lor b \lor c$ is \textbf{true}
              if one or more of the propositions is true; it is \textbf{only false}
              if all propositions are false.
          \item A compound proposition $\neg a$ is \textbf{true} only
              if the proposition is false; it is only \textbf{false} if
              the proposition is true.
      \end{enumerate}

  \end{intro}
  
  \begin{center}
    \vspace{1cm}
    \includegraphics[height=7cm]{images/u1e1-crash.png}
  \end{center}

  \newpage

  \stepcounter{question}
  \begin{questionNOGRADE}{\thequestion}
      Given the following compound propositions and proposition values,
      write out whether the full compound proposition is \textbf{true} or \textbf{false}.
  \end{questionNOGRADE}

  \paragraph{a. a AND b:} ~\\

  \begin{tabular}{ | l  c | c | p{6cm} | }
      \hline
      & \textbf{Compound} & \textbf{Values} & \textbf{Result} \\ \hline
      a. &        $a \land b$ &       $a = true, b = true$ &       TRUE     \\ \hline
      b. &        $a \land b$ &       $a = true, b = false$ &      FALSE    \\ \hline
      c. &        $a \land b$ &       $a = false, b = true$ &               \\ & & & \\ \hline
      d. &        $a \land b$ &       $a = false, b = false$ &              \\ & & & \\ \hline
  \end{tabular}

  \paragraph{b. a OR b:} ~\\

  \begin{tabular}{ | l  c | c | p{6cm} | }
      \hline
      & \textbf{Compound} & \textbf{Values} & \textbf{Result} \\ \hline
      e. &        $a \lor b$ &       $a = true, b = true$ &     TRUE \\ \hline
      f. &        $a \lor b$ &       $a = true, b = false$ &    \\ & & & \\ \hline
      g. &        $a \lor b$ &       $a = false, b = true$ &    \\ & & & \\ \hline
      h. &        $a \lor b$ &       $a = false, b = false$ &   \\ & & & \\ \hline
  \end{tabular}

  \paragraph{c. Combinations:} ~\\

  \begin{tabular}{ | l  c | c | p{6cm} | }
      \hline
      & \textbf{Compound} & \textbf{Values} & \textbf{Result} \\ \hline
      i. &        $a \land \neg b$ &       $a = true, b = false$ &   \\ & & & \\ \hline
      j. &        $a \lor \neg b$ &        $a = false, b = true$ &   \\ & & & \\ \hline
      k. &        $\neg a \land b$ &       $a = false, b = true$ &   \\ & & & \\ \hline
      l. &        $\neg a \lor b$ &        $a = false, b = false$ &  \\ & & & \\ \hline
  \end{tabular}


% -------------------------------------------------------------------- %
  \newpage
  \begin{intro}{Propositional variables}
    While working with propositional logic in discrete math, it is common to use a single-letter variable name in place of a whole statement.
    For example, instead of ``The sky is blue'', we might use the variable $s$ instead. (However, when writing programs, you should be using
    descriptive variable names!!)
  \end{intro}

  \stepcounter{question}
  \begin{questionNOGRADE}{\thequestion}
      For the following, ``translate" the following English statements
      into compound propositions.

      ~\\
      $m$: There is milk in the fridge \tab
      $e$: There are eggs in the fridge \\
      $c$: There is cheese in the fridge ~\\

      \begin{enumerate}
          \item   There is milk in the fridge, or there is cheese in the fridge.                      ~\\ \solution{ $m \lor c$ }{}
          \item   There is milk in the fridge but (and) there are no eggs in the fridge.              \vspace{1.2cm}
          \item   There is no milk in the fridge and there is no cheese in the fridge.                \vspace{1.2cm}
          \item   There is milk in the fridge and there is either cheese or eggs in the fridge.       \vspace{1.2cm}
          \item   It is not true that... there is milk or cheese in the fridge.                       ~\\ \solution{ $\neg ( m \lor c )$ }{}
          \item   It is not true that... there is milk and cheese in the fridge.                      \vspace{1.2cm}
          \item   There is milk in the fridge, or there is cheese in the fridge, but (and) not both.  \vspace{1.2cm}
      \end{enumerate}
  \end{questionNOGRADE}


% -------------------------------------------------------------------- %
  \newpage
  \begin{intro}{Truth Tables}
      When we're working with compound propositional statements,
      the result of the compound depends on the true/false values
      of each proposition it is built up of.
      ~\\
      We can diagram out all possible states of a compound proposition
      by using a \textbf{truth table}. In a truth table, we list
      all propositional variables first on the left, as well as
      all possible combinations of their states, and then
      the compound statement's result on the right.

      \begin{tabular}{ p{5cm} p{5cm} }
        \textbf{Truth table for AND:} & 
        \begin{tabular}{ | c | c || c | }          \hline
            $p$       & $q$       & $p \land q$     \\ \hline
            \true     & \true     & \true           \\ \hline
            \true     & \false    & \false          \\ \hline
            \false    & \true     & \false          \\ \hline
            \false    & \false    & \false          \\ \hline
        \end{tabular}        
        \\ \\
        \textbf{Truth table for OR:} &
        \begin{tabular}{ | c | c || c | }      \hline
            $p$     & $q$       & $p \lor q$    \\ \hline
            \true   & \true     & \true         \\ \hline
            \true   & \false    & \true         \\ \hline
            \false  & \true     & \true         \\ \hline
            \false  & \false    & \false        \\ \hline
        \end{tabular}        
        \\ \\
        \textbf{Truth table for NOT:} &
        \begin{tabular}{ | c | | c | }  \hline
            $p$     & $\neg p$          \\ \hline
            \true   & \false            \\ \hline
            \false  & \true             \\ \hline
        \end{tabular}
      \end{tabular}
  \end{intro}
  
  \begin{intro}{Truth table ordering}
      When you're building out truth tables, there is a specific order
      you should write out the ``T" and ``F" states.
      Begin with all ``T" values first, and work your way down to
      all ``F" values. As you go, change the right-most state
      from ``T" to ``F", working your way from right-to-left.

      ~\\
      So with two variables: \\ ``TT", ``TF", ``FT", ``FF".
  \end{intro}
  
    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Using the rules above, write out all the states for a
        truth table with three propositional variables. ~\\
        
        \begin{center}
            \begin{tabular}{ | p{2cm} | p{2cm} | p{2cm} | }
                \hline
                $p$ & $q$ & $r$   \\ \hline
                T & T & T         \\ \hline
                T & T & F         \\ \hline
                T &  &          \\ \hline
                T & F & F         \\ \hline
                F &  &          \\ \hline
                F &  &          \\ \hline
                F &  &          \\ \hline
                F & F & F         \\ \hline
            \end{tabular}
        \end{center}
    \end{questionNOGRADE}
  
  \newpage
  \begin{intro}{Validating our logic with Truth Tables}
      
      Lets say we had the statement ``There is milk in the fridge, or there is cheese in the fridge, but (and) not both.''
      and a student comes up with the solution: $m \lor c$.   
      Let's validate it with a truth table. ~\\
      
      First, we want the outcome to be \textbf{true} for these scenarios:
      
      \begin{itemize}
        \item   Milk is in the fridge, but not cheese ($m$ is true, $c$ is false), OR
        \item   Milk is not in the fridge, but cheese is ($m$ is false, $c$ is true).
      \end{itemize}
      
      And these two scenarios should result in \textbf{false}:
      
      \begin{itemize}
        \item   Milk is in the fridge and cheese is in the fridge ($m$ is true, $c$ is true).
        \item   Milk is not in the fridge and cheese is not in the fridge ($m$ is false, $c$ is false).
      \end{itemize}
      
      If these scenarios are true, the resulting truth table should look like this: ~\\
    
      \begin{tabular}{ | c | c || c | l | }             \hline
          $p$     & $q$       & LOGICALLY CORRECT & Translation                                     \\ \hline
          \true   & \true     & \false            & there is milk and there is cheese               \\ \hline
          \true   & \false    & \true             & there is milk but no cheese                     \\ \hline
          \false  & \true     & \true             & there is no milk but there is cheese            \\ \hline
          \false  & \false    & \false            & there is no milk and no cheese                  \\ \hline
      \end{tabular} 
      
      ~\\
      
      However, once we fill out the truth table for $m \lor c$, we get this: ~\\
      
      \begin{tabular}{ | c | c || c | l | }             \hline
          $p$     & $q$       & $m \lor c$ & Translation                                            \\ \hline
          \true   & \true     & \true      & there is milk and there is cheese                      \\ \hline
          \true   & \false    & \true      & there is milk but no cheese                            \\ \hline
          \false  & \true     & \true      & there is no milk but there is cheese                   \\ \hline
          \false  & \false    & \false     & there is no milk and no cheese                         \\ \hline
      \end{tabular} 
      ~\\ ~\\
      
      So the student's solution is incorrect, because while two scenarios are correct (milk and no cheese / cheese and no milk),
      the scenario where milk and cheese are both in the fridge comes out to true.
      Therefore, $m \lor c$ \textit{ \textbf{ does not } } accurately reflect the statement
      ``There is milk in the fridge, or there is cheese in the fridge, but (and) not both''.
  \end{intro}


  \newpage
  \stepcounter{question}
  \begin{questionNOGRADE}{\thequestion}
      Given the statement ``There is milk in the fridge, or there is cheese in the fridge, but (and) not both'' and the variables ~\\
      
      \begin{tabular}{l l}
        $m$: & There is milk in the fridge \\
        $e$: & There are eggs in the fridge \\
        $c$: & There is cheese in the fridge \\
      \end{tabular}
      
      ~\\ Three students came up with these solutions:

      \begin{enumerate}
        \item   $m \lor \neg c$
        \item   $(m \lor c) \land \neg (m \land c)$
        \item   $(m \land \neg c) \lor (\neg m \land c)$
      \end{enumerate}
      
      If their solution is right, their truth table should result in this:
      
      \begin{tabular}{ | c | c || c | l | }             \hline
          $p$     & $q$       & LOGICALLY CORRECT & Translation                                     \\ \hline
          \true   & \true     & \false            & there is milk and there is cheese               \\ \hline
          \true   & \false    & \true             & there is milk but no cheese                     \\ \hline
          \false  & \true     & \true             & there is no milk but there is cheese            \\ \hline
          \false  & \false    & \false            & there is no milk and no cheese                  \\ \hline
      \end{tabular} 
      
      \vspace{1cm}
      
      Fill out each truth table and verify whether each student's logic is correct.
      
      \vspace{1cm}
      
      \begin{tabular}{ | c | c || c || c | p{6cm} | }                                                                                                 \hline
                  &           & Helper column & Student answer                               & Translation                                            \\
          $m$     & $c$       & $\neg c$      & $m \lor \neg c$                              &                                                        \\ \hline
          \true   & \true     & \false        &                                              & there is milk and there is cheese                      
          
          \\ \hline
          \true   & \false    & \true         &                                              & there is milk but no cheese                            
          
          \\ \hline
          \false  & \true     & \false        &                                              & there is no milk but there is cheese                   
          
          \\ \hline
          \false  & \false    & \true         &                                              & there is no milk and no cheese                         
          
          \\ \hline
      \end{tabular} 
      
      \vspace{1cm}
      
      Was the solution correct? \tab $\ocircle$ Yes \tab $\ocircle$ No
      
      \vspace{1cm}
      
      (Continued...)
      
      \newpage
      
      \begin{tabular}{ | c | c || c | c | c || c | p{3.5cm} | }             \hline
                  &           & \multicolumn{3}{c||}{Helper columns}                  & Student answer                               & Translation                                            \\
          $m$     & $c$       & $m \lor c$  & $m \land c$   & $\neg (m \land c)$      & $(m \lor c) \land \neg (m \land c)$          &                                      \\ \hline
          \true   & \true     &             &               &                         &                                              & there is milk and there is cheese               \\ \hline
          \true   & \false    &             &               &                         &                                              & there is milk but no cheese                     \\ \hline
          \false  & \true     &             &               &                         &                                              & there is no milk but there is cheese            \\ \hline
          \false  & \false    &             &               &                         &                                              & there is no milk and no cheese                  \\ \hline
      \end{tabular}
      
      \vspace{1cm}
      
      Was the solution correct? \tab $\ocircle$ Yes \tab $\ocircle$ No
      
      \vspace{1cm}
      
      \begin{tabular}{ | c | c || p{4cm} || c | p{3.5cm} | }             \hline
                  &           & Helper columns (fill in)                & Student answer                               & Translation                                            \\
          $m$     & $c$       &                                         & $(m \land \neg c) \lor (\neg m \land c)$     &                                      \\ \hline
          \true   & \true     &                                         &                                              & there is milk and there is cheese               \\ \hline
          \true   & \false    &                                         &                                              & there is milk but no cheese                     \\ \hline
          \false  & \true     &                                         &                                              & there is no milk but there is cheese            \\ \hline
          \false  & \false    &                                         &                                              & there is no milk and no cheese                  \\ \hline
      \end{tabular} 

      \vspace{1cm}
      
      Was the solution correct? \tab $\ocircle$ Yes \tab $\ocircle$ No
      
      \vspace{1cm}
  \end{questionNOGRADE}
  
% -------------------------------------------------------------------- %
  \newpage
  \begin{intro}{Logical equivalence}
      Whenever the final columns of the truth tables for two propositions $p$ and $q$ are the same,
      we say that $p$ and $q$ are \textbf{logically equivalent}, and we write: $p \equiv q$.
      \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}
  \end{intro}

  \stepcounter{question}
  \begin{questionNOGRADE}{\thequestion}
      Use a truth table to show that the compound propositions,

      \begin{center}
      $ \neg p \land \neg q $
      \tab and \tab
      $ \neg ( p \lor q ) $
      \end{center}
      are logically equivalent. The final two columns are the compound propositions above
      ~\\
      \begin{center} \large
          \begin{tabular}{ | c | c | | c | c | c || c || c | } \hline
                        &         & \multicolumn{3}{c||}{Helper columns}              & \multicolumn{2}{c|}{Compound propositions} \\
              $p$       & $q$     & $\neg p$      & $\neg q$    & $(p \lor q)$        & $ \neg p \land \neg q $         & $ \neg ( p \lor q ) $   \\ \hline
              \true     & \true   & \false        & \false      &                     &                                 &                         \\ \hline
              \true     & \false  & \false        & \true       &                     &                                 &                         \\ \hline
              \false    & \true   & \true         & \false      &                     &                                 &                         \\ \hline
              \false    & \false  & \true         & \true       &                     &                                 &                         \\ \hline
          \end{tabular} 
          \normalsize
      \end{center}       
  \end{questionNOGRADE}



% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
% -------------------------------------------------------------------- %
\newpage
\section*{Solutions}

\solution{

\begin{enumerate}
  \item  ~\\
      \begin{tabular}{l l | c | c}
          & \textbf{Statement} & \textbf{Proposition?} & \textbf{Truth value?} \\ \hline
          a. &    10 + 20 = 30          & yes               & true                                \\ & & \\ \hline                
          b. &    Is $4 < 5$?           & no; question      & -                                   \\ & & \\ \hline
          c. &    It is raining today.  & yes               & true/false (depends on today.)      \\ & & \\ \hline
          d. &    $x \cdot y$ is odd.   & no; what is x, y? & -                                   \\ & & \\ \hline
          e. &    $5 + 1$ is odd.       & yes               & false                               \\ & & \\ \hline
          f. &    Go to the store.      & no; is a command  & -                                   \\ & & \\ \hline
      \end{tabular}
  
  \item 
    ~\\ a AND b: ~\\ ~\\
      \begin{tabular}{ | l  c | c | p{6cm} | }
          \hline
          & \textbf{Compound} & \textbf{Values} & \textbf{Result} \\ \hline
          a. &        $a \land b$ &       $a = true, b = true$      &     $ a \land b \equiv true \land true \equiv true $      \\ \hline
          b. &        $a \land b$ &       $a = true, b = false$     &     $ a \land b \equiv true \land false \equiv false $    \\ \hline
          c. &        $a \land b$ &       $a = false, b = true$     &     $ a \land b \equiv false \land true \equiv false $    \\ \hline
          d. &        $a \land b$ &       $a = false, b = false$    &     $ a \land b \equiv false \land false \equiv false $   \\ \hline
      \end{tabular}
    
    ~\\ a OR b: ~\\ ~\\
      \begin{tabular}{ | l  c | c | p{6cm} | }
          \hline
          & \textbf{Compound} & \textbf{Values} & \textbf{Result} \\ \hline
          e. &        $a \lor b$ &       $a = true, b = true$       &   $ a \lor b \equiv true \lor true \equiv true $  \\ \hline
          f. &        $a \lor b$ &       $a = true, b = false$      &   $ a \lor b \equiv true \lor false \equiv true $  \\ \hline
          g. &        $a \lor b$ &       $a = false, b = true$      &   $ a \lor b \equiv false \lor true \equiv true $  \\ \hline
          h. &        $a \lor b$ &       $a = false, b = false$     &   $ a \lor b \equiv false \lor false \equiv false $  \\ \hline
      \end{tabular}
      
    \newpage
    ~\\ Combinations: ~\\ ~\\
      \begin{tabular}{ | l  c | c | p{6cm} | }
          \hline
          & \textbf{Compound} & \textbf{Values} & \textbf{Result}                             \\ \hline
          i. &        $a \land \neg b$ &       $a = true, b = false$ &   
                      $a \land \neg b$ 
                      
                      $\equiv true \land \neg false$                      
                
                      $\equiv true \land true$
                      
                      $\equiv true$          
                      \\ \hline
          j. &        $a \lor \neg b$ &        $a = false, b = true$ &   
                      $a \lor \neg b$
                      
                      $\equiv false \lor \neg true$
                      
                      $\equiv false \lor false$
                      
                      $\equiv false$                               
                      \\ \hline
                
          k. &        $\neg a \land b$ &       $a = false, b = true$ &                        
                      $\neg a \land b$
                      
                      $\equiv \neg false \land true$
                      
                      $\equiv true \land true$
                      
                      $\equiv true$                
                      \\ \hline
                
          l. &        $\neg a \lor b$ &        $a = false, b = false$ &                       
                      $\neg a \lor b$
                      
                      $\equiv \neg false \lor false$
                      
                      $\equiv true \lor false$
                      
                      $\equiv true$
                      
                      \\ \hline
      \end{tabular}
  
  \item 
      \begin{enumerate}
          \item   There is milk in the fridge, or there is cheese in the fridge.                      ~\\ $m \lor c$
          \item   There is milk in the fridge but (and) there are no eggs in the fridge.              ~\\ $m \land \neg e$
          \item   There is no milk in the fridge and there is no cheese in the fridge.                ~\\ $\neg m \land \neg c$
          \item   There is milk in the fridge and there is either cheese or eggs in the fridge.       ~\\ $m \land (c \lor e)$
          \item   It is not true that... there is milk or cheese in the fridge.                       ~\\ $\neg ( m \lor c )$
          \item   It is not true that... there is milk and cheese in the fridge.                      ~\\ $\neg ( m \land c)$
          \item   There is milk in the fridge, or there is cheese in the fridge, but (and) not both.  ~\\ $( m \lor c ) \land \neg ( m \land c )$
      \end{enumerate}

  \newpage
  \item ~\\
      \begin{tabular}{ | p{2cm} | p{2cm} | p{2cm} | }
          \hline
          $p$ & $q$ & $r$   \\ \hline
          \true & \true & \true         \\ \hline
          \true & \true & \false         \\ \hline
          \true & \false & \true         \\ \hline
          \true & \false & \false         \\ \hline
          \false & \true & \true         \\ \hline
          \false & \true & \false         \\ \hline
          \false & \false & \true         \\ \hline
          \false & \false & \false         \\ \hline
      \end{tabular}
      (Notice the pattern!)

  \item   Solution 1:
  
      \begin{tabular}{ | c | c || c || c | p{6cm} | }                                                                                                 \hline
                  &           & Helper column & Student answer                               & Translation                                            \\
          $m$     & $c$       & $\neg c$      & $m \lor \neg c$                              &                                                        \\ \hline
          \true   & \true     & \false        & \true                                        & there is milk and there is cheese                      
          
          \\ \hline
          \true   & \false    & \true         & \true                                        & there is milk but no cheese                            
          
          \\ \hline
          \false  & \true     & \false        & \false                                       & there is no milk but there is cheese                   
          
          \\ \hline
          \false  & \false    & \true         & \true                                        & there is no milk and no cheese                         
          
          \\ \hline
      \end{tabular}
      ~\\ Was the solution correct? \tab \textbf{No}
      
  ~\\ Solution 2:
  
      \begin{tabular}{ | c | c || c | c | c || c | p{3.5cm} | }             \hline
                  &           & \multicolumn{3}{c||}{Helper columns}                  & Student answer                               & Translation                                            \\
          $m$     & $c$       & $m \lor c$  & $m \land c$   & $\neg (m \land c)$      & $(m \lor c) \land \neg (m \land c)$          &                                      \\ \hline
          \true   & \true     & \true       & \true         & \false                  & \false                                       & there is milk and there is cheese               \\ \hline
          \true   & \false    & \true       & \false        & \true                   & \true                                        & there is milk but no cheese                     \\ \hline
          \false  & \true     & \true       & \false        & \true                   & \true                                        & there is no milk but there is cheese            \\ \hline
          \false  & \false    & \false      & \false        & \true                   & \false                                       & there is no milk and no cheese                  \\ \hline
      \end{tabular}
      ~\\ Was the solution correct? \tab \textbf{Yes}
      
      
  \newpage
  ~\\ Solution 3:
  
      \begin{tabular}{ | c | c || c | c || c | p{5cm} | }             \hline
                  &           & \multicolumn{2}{c||}{Helper columns}    & Student answer                               & Translation                                            \\
          $m$     & $c$       & $m \land \neg c$ & $\neg m \land c$     & $(m \land \neg c) \lor (\neg m \land c)$     &                                      \\ \hline
          \true   & \true     & \false           & \false               & \false                                       & there is milk and there is cheese               \\ \hline
          \true   & \false    & \true            & \false               & \true                                        & there is milk but no cheese                     \\ \hline
          \false  & \true     & \false           & \true                & \true                                        & there is no milk but there is cheese            \\ \hline
          \false  & \false    & \false           & \false               & \false                                       & there is no milk and no cheese                  \\ \hline
      \end{tabular}
      ~\\ Was the solution correct? \tab \textbf{Yes} 
      
  
  \item ~\\
    \begin{tabular}{ | c | c | | c | c | c || c || c | } \hline
                  &         & \multicolumn{3}{c||}{Helper columns}              & \multicolumn{2}{c|}{Compound propositions} \\
        $p$       & $q$     & $\neg p$      & $\neg q$    & $(p \lor q)$        & $ \neg p \land \neg q $         & $ \neg ( p \lor q ) $   \\ \hline
        \true     & \true   & \false        & \false      & \true               & \false                          & \false                  \\ \hline
        \true     & \false  & \false        & \true       & \true               & \false                          & \false                  \\ \hline
        \false    & \true   & \true         & \false      & \true               & \false                          & \false                  \\ \hline
        \false    & \false  & \true         & \true       & \false              & \true                           & \true                   \\ \hline
    \end{tabular} 
\end{enumerate}

}{}

\input{BASE-FOOT}


