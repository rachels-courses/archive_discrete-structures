\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {HW1A Logic - Propositional Logic}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 1.3}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 1.2}
\newcommand{\laTextbookC}   {ZyBooks: Chapter 1.1, 1.2, 1.4}
\newcounter{question}

\toggletrue{answerkey}     	\togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-HOMEWORK}

% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Propositional Logic}

\notonkey{
    \begin{intro}{\ }
        A \textbf{proposition} is a statement which has truth value: it is either true (T) or false (F).
        \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}

        The statement doesn't have to necessarily be TRUE, it could also
        be FALSE, but it has to be unambiguously so. ~\\

        \begin{tabular}{ | l | l | l | }
            \hline
            \textbf{Statement} & \textbf{Proposition?} & \textbf{Result?}
            \\ \hline
            2 + 3 = 5 & Yes & True
            \\ \hline
            2 + 2 = 5 & Yes & False
            \\ \hline
            This class has 30 students & Yes & False\footnote{Probably :)}
            \\ \hline
            Is the movie good? & No &
            \\ \hline
        \end{tabular}
    \end{intro}
}{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}

        \small For the following, mark whether the statement is a \textbf{proposition} and,
        if it is, mark whether it is \textbf{true} or \textbf{false}. If it is not a proposition,
        leave the last column blank or write ``n/a''.

        \begin{center}
            \begin{tabular}{l l | c | c}
                & \textbf{statement} & \textbf{is proposition?} & \textbf{is true/false?}
                \\ \hline
                a. & 10 + 20 = 30
                & \solution{yes}{}
                & \solution{true}{}
                \\ \hline

                b.
                & 2 times an integer is always even.
                & \solution{yes}{}
                & \solution{true}{}
                \\ \hline

                c. & Is $x < 5$?
                & \solution{no}{}
                &
                \\ \hline

                d. & $5 + 1$ is odd.
                & \solution{yes}{}
                & \solution{false}{}
                \\ \hline

                e. & Run!
                & \solution{no}{}
                &
                \\ \hline

                f. & How many students are in the class?
                & \solution{no}{}
                &
                \\
            \end{tabular}
        \end{center}

    \end{questionNOGRADE}
    \normalsize


    \notonkey{
    \paragraph{Solution to Question 1}

    \newpage
        \begin{center}
            \begin{tabular}{l l | c | c}
                & \textbf{statement} & \textbf{is proposition?} & \textbf{is true/false?}
                \\ \hline
                a. & 10 + 20 = 30
                & \color{red} Proposition \color{black}
                & \color{red} True \color{black}
                \\ \hline

                b. & 2 times an integer is always even.
                & \color{red} Proposition \color{black}
                & \color{red} True \color{black}
                \\ \hline

                c. & Is $x < 5$?
                & \color{red} Not a proposition \color{black}
                & \color{red} n/a \color{black}
                \\ \hline

                d. & $5 + 1$ is odd.
                & \color{red} Proposition \color{black}
                & \color{red} False \color{black}
                \\ \hline

                e. & Run!
                & \color{red} Not a proposition. \color{black}
                & \color{red} n/a \color{black}
                \\ \hline

                f. & How many students are in the class?
                & \color{red} Not a proposition \color{black}
                & \color{red} n/a \color{black}
                \\ \hline
            \end{tabular}
        \end{center}
    }{}

    \notonkey{
    \subsection*{Compound Propositions and Logical Operators}
    }{}

	\notonkey{
    \begin{intro}{\ }
        We can also create a \textbf{compound proposition} using the logical operators
        for AND $\land$, OR $\lor$, and NOT $\neg$. When we're writing out
        a compound proposition, we will usually assign each proposition a
        \textbf{propositional variable}.

        ~\\ If we have the propositions
        $p$ is the proposition ``Paul is taking discrete math",
        $q$ is the proposition, ``Paul has a calculator",
        then we can build the compound propositions like:
        \begin{itemize}
            \item[] $p \land q$: Paul is taking discrete math and Paul has a calculator
            \item[] $p \lor q$: Paul is taking discrete math or Paul has a calculator
            \item[] $\neg p$: Paul is NOT taking discrete math
            \item[] $\neg q$: Paul does NOT have a calculator
        \end{itemize}

        The result of a compound proposition depends on the value of
        each proposition it is made up of:

        \begin{enumerate}
            \item A compound proposition $a \land b \land c$ is \textbf{only true}
                if all propositions are true; it will be \textbf{false} if
                one or more of the propositions is false.
            \item A compound proposition $a \lor b \lor c$ is \textbf{true}
                if one or more of the propositions is true; it is \textbf{only false}
                if all propositions are false.
            \item A compound proposition $\neg a$ is \textbf{true} only
                if the proposition is false; it is only \textbf{false} if
                the proposition is true.
        \end{enumerate}
    \end{intro}
    }{}

    \notonkey{
    \newpage
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Given the following compound propositions and proposition values,
        write out whether the full compound proposition is \textbf{true} or \textbf{false}.

        \large

        \paragraph{a. a AND b:} ~\\

        \begin{tabular}{ | l  c | c | p{4cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            a. &        $a \land b$ &       $a = true, b = true$ &      TRUE   \\ \hline
            b. &        $a \land b$ &       $a = true, b = false$ &     \solution{FALSE}{} \\ \hline
            c. &        $a \land b$ &       $a = false, b = true$ &     \solution{FALSE}{} \\ \hline
            d. &        $a \land b$ &       $a = false, b = false$ &    FALSE   \\ \hline
        \end{tabular}

        \paragraph{b. a OR b:} ~\\

        \begin{tabular}{ | l  c | c | p{4cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            e. &        $a \lor b$ &       $a = true, b = true$ &   \solution{TRUE}{}      \\ \hline
            f. &        $a \lor b$ &       $a = true, b = false$ &  \solution{TRUE}{}     \\ \hline
            g. &        $a \lor b$ &       $a = false, b = true$ &  \solution{TRUE}{}     \\ \hline
            h. &        $a \lor b$ &       $a = false, b = false$ & \solution{FALSE}{}   \\ \hline
        \end{tabular}

        \paragraph{c. Combinations:} ~\\

        \begin{tabular}{ | l  c | c | p{4cm} | }
            \hline
            & \textbf{Compound} & \textbf{Values} & \textbf{Result}
            \\ \hline

            i. &        $a \land \neg b$ &       $a = true, b = false$ &    \solution{TRUE}{}    \\ \hline
            j. &        $a \lor \neg b$ &        $a = false, b = true$ &    \solution{FALSE}{}   \\ \hline

            k. &        $\neg a \land b$ &       $a = false, b = true$ &    \solution{TRUE}{}   \\ \hline
            l. &        $\neg a \lor b$ &        $a = false, b = false$ &   \solution{TRUE}{}  \\ \hline
        \end{tabular}

    \end{questionNOGRADE}
    \normalsize


    \newpage

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For the following propositions...

        \begin{center}
            $m$: There is milk in the fridge \tab $c$: There is cheese in the fridge
        \end{center}

        ~\\
        ...``Translate" the following English statements
        into compound propositions. Use parentheses when mixing $\land$ and $\lor$ and $\neg$ operators.

        \begin{enumerate}
            \item[a.]   There is milk in the fridge, OR there is cheese in the fridge.
                        \solution{~\\ $m \lor c$}{\vspace{2cm} }
            \item[b.]   There is milk in the fridge, AND there is NOT cheese in the fridge.
                        \solution{~\\ $m \land \neg c$ }{\vspace{2cm} }
            \item[c.]   There is cheese in the fridge, AND there is NOT milk in the fridge.
                        \solution{~\\ $c \land \neg m$ }{\vspace{2cm} }
            \item[d.]   It is not true that: milk AND cheese are in the fridge.
                        \solution{~\\ $\neg (m \land c)$ }{\vspace{2cm} }
            \item[e.]   There is NOT milk in the fridge, AND there is NOT cheese in the fridge.
                        \solution{$ ~\\\neg m \land \neg c$}{\vspace{2cm} }
        \end{enumerate}

    \end{questionNOGRADE}

    \notonkey{
    \newpage

    \paragraph{Solution for Question 3}

	\begin{enumerate}
		\item[a.]   There is milk in the fridge, OR there is cheese in the fridge.
					~\\ \color{red} $m \lor c$ \color{black}

		\item[b.]   There is milk in the fridge, AND there is NOT cheese in the fridge.
					~\\ \color{red} $m \land \neg c$ \color{black}

		\item[c.]   There is cheese in the fridge, AND there is NOT milk in the fridge.
					~\\ \color{red} $c \land \neg m$ \color{black}

		\item[d.]   It is not true that: milk AND cheese are in the fridge.
					~\\ \color{red} $\neg( m \land c )$ \color{black}

		\item[e.]   There is NOT milk in the fridge, AND there is NOT cheese in the fridge.
					~\\ \color{red} $\neg m \land \neg c$ \color{black}
	\end{enumerate}

    }{}


    \notonkey{
    \subsection*{Truth Tables}

    \begin{intro}{\ }
        When we're working with compound propositional statements,
        the result of the compound depends on the true/false values
        of each proposition it is built up of.
        ~\\
        We can diagram out all possible states of a compound proposition
        by using a \textbf{truth table}. In a truth table, we list
        all propositional variables first on the left, as well as
        all possible combinations of their states, and then
        the compound statement's result on the right.

        ~\\
        \begin{center}
            \begin{tabular}{ c c c }

				AND & OR & NOT \\

                \begin{tabular}{ | c | c || c | }
                    \hline
                    $p$ & $q$ & $p \land q$ \\ \hline
                    \true & \true & \true \\ \hline
                    \true & \false & \false \\ \hline
                    \false & \true & \false \\ \hline
                    \false & \false & \false \\ \hline

                \end{tabular}
                &

                \begin{tabular}{ | c | c || c | }
                    \hline
                    $p$ & $q$ & $p \lor q$ \\ \hline
                    \true & \true & \true \\ \hline
                    \true & \false & \true \\ \hline
                    \false & \true & \true \\ \hline
                    \false & \false & \false \\ \hline

                \end{tabular}
                &

                \begin{tabular}{ | c || c | }
                    \hline
                    $p$ & $\neg p$ \\ \hline
                    \true & \false \\ \hline
                    \false & \true \\ \hline

                \end{tabular}

            \end{tabular}
        \end{center}
    \end{intro}

    \newpage

    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Complete the following truth tables. \large

        \begin{enumerate}
            \item[a.] ~\\
                \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{3cm} | }
                    \hline
                    $p$ & $q$ & $\neg q$ & $p \land \neg q$ \\ \hline
                    T & T & F & \solution{F}{}
                    \\ \hline
                    T & F & T & \solution{T}{}
                    \\ \hline
                    F & T & \solution{F}{} & \solution{F}{}
                    \\ \hline
                    F & F & \solution{T}{} & \solution{F}{}
                    \\ \hline
                \end{tabular} ~\\

            \item[b.] ~\\
                \begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{2cm} | p{3cm} | }
                    \hline
                    $p$ & $q$ & $\neg p$ & $\neg q$ & $\neg p \lor \neg q$ \\ \hline
                    T & T & \solution{F}{} & \solution{F}{} & \solution{F}{}
                    \\ \hline
                    T & F & \solution{F}{} & \solution{T}{} & \solution{T}{}
                    \\ \hline
                    F & T & \solution{T}{} & \solution{F}{} & \solution{T}{}
                    \\ \hline
                    F & F & \solution{T}{} & \solution{T}{} & \solution{T}{}
                    \\ \hline
                \end{tabular}
        \end{enumerate}
        \normalsize
    \end{questionNOGRADE}

    \notonkey
    {
    \begin{intro}{\ }
        When you're building out truth tables from scratch, there is a specific order
        you should write out the ``T" and ``F" states.
        Begin with all ``T" values first, and work your way down to
        all ``F" values first. As you go, change the right-most state
        from ``T" to ``F", working your way from right-to-left.

        ~\\
        So with two variables: \\ ``TT", ``TF", ``FT", ``FF".

        ~\\~\\
        And three variables: \\
        ``TTT'', ``TTF'', ``TFT'', ``TFF'', \\
        ``FTT'', ``FTF'', ``FFT'', ``FFF''
    \end{intro}

	\newpage

	\paragraph{Solution for Question 4}

	\begin{enumerate}
		\item[a.] ~\\
			\begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{3cm} | }
				\hline
				$p$ & $q$ & $\neg q$ & $p \land \neg q$ \\ \hline
				T & T & F & F \\ \hline
				T & F & T & T \\ \hline
				F & T & F & F \\ \hline
				F & F & T & F \\ \hline
			\end{tabular} ~\\

		\item[b.] ~\\
			\begin{tabular}{ | p{1cm} | p{1cm} || p{2cm} | p{2cm} | p{3cm} | }
				\hline
				$p$ & $q$ & $\neg p$ & $\neg q$ & $\neg p \lor \neg q$ \\ \hline
				T & T & F & F & F \\ \hline
				T & F & F & T & T \\ \hline
				F & T & T & F & T \\ \hline
				F & F & T & T & T \\ \hline
			\end{tabular}
	\end{enumerate}
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Using the rules previously stated, write out all the states for a
        truth table with three propositional variables. ~\\ \large

        \begin{center}
            \begin{tabular}{ | p{2cm} | p{2cm} | p{2cm} | }
                \hline
                $p$ & $q$           & $r$
                \\ \hline
                T & T & T
                \\ \hline
                T & T & F
                \\ \hline
                T & \solution{F}{}  & \solution{T}{}
                \\ \hline
                T & F & F
                \\ \hline
                F & \solution{T}{} & \solution{T}{}
                \\ \hline
                F & \solution{T}{} & \solution{F}{}
                \\ \hline
                F & \solution{F}{} & \solution{T}{}
                \\ \hline
                F & F & F
                \\ \hline
            \end{tabular}
        \end{center}
        \normalsize
    \end{questionNOGRADE}

    \newpage

    \notonkey{
    \begin{intro}{\ }
        Whenever the final columns of the truth tables for two propositions $p$ and $q$ are the same,
        we say that $p$ and $q$ are \textbf{logically equivalent}, and we write: $p \equiv q$.
        \footnote{From https://en.wikibooks.org/wiki/Discrete\_Mathematics/Logic}
    \end{intro}
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        Use a truth table to show that the compound propositions,

        \begin{center}
        $ \neg p \land \neg q $
        \tab and \tab
        $ \neg ( p \lor q ) $
        \end{center}
        are logically equivalent. The \textbf{final two columns} are the compound propositions above
        and should have the same values
        ~\\ \large
        \begin{center}
            \begin{tabular}{ c | c || c | c || c || c || c }
                $p$ & $q$   & $\neg p$          & $\neg q$          & $(p \lor q)$      & $\neg p \land \neg q$     & $\neg (p \lor q)$
                \\ \hline
                T   & T     & \solution{F}{}    & \solution{F}{}    & \solution{T}{}    & \solution{F}{}            & \solution{F}{}
                \\
                T   & F     & \solution{F}{}    & \solution{T}{}    & \solution{T}{}    & \solution{F}{}            & \solution{F}{}
                \\
                F   & T     & \solution{T}{}    & \solution{F}{}    & \solution{T}{}    & \solution{F}{}            & \solution{F}{}
                \\
                F   & F     & \solution{T}{}    & \solution{T}{}    & \solution{F}{}    & \solution{T}{}            & \solution{T}{}
            \end{tabular}

        \end{center}
        \normalsize
    \end{questionNOGRADE}

    \notonkey{
    \newpage
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
        For Question 3, we had the propositions: ~\\
        \begin{center}
            $m$: There is milk in the fridge \tab
            $c$: There is cheese in the fridge
        \end{center}

        ~\\
        a. Build a statement for:

        \begin{center}
            \textit{ There is milk in the fridge, OR there is cheese in the fridge, \\
            BUT not both at the same time. }
        \end{center}

        \solution{
            There are multiple ways you could write this, such as:\\

            \begin{center}
                $(m \lor c) \land \neg(m \land c)$ \\
                or \\
                $(m \land \neg c) \lor (\neg m \land c)$
            \end{center}

        }{ \vspace{2cm} }

        ~\\
        b. Validate your statement using the truth table:

        \notonkey{
        ~\\
        \begin{tabular}{c | c | | p{11cm}  }
            & & Your statement from (a)... (write in)
            \\ & & \\ $m$ & $c$ & \\ \hline
            & & \\ T & T & \\ & & \\ \hline
            & & \\ T & F & \\ & & \\ \hline
            & & \\ F & T & \\ & & \\ \hline
            & & \\ F & F & \\ & &
        \end{tabular}
        }{}

        \solution{
            ~\\
            Example 1: $(m \lor c) \land \neg(m \land c)$

            ~\\
            \begin{tabular}{c | c || c || c || c || c }
                $m$ & $c$   & $m \lor c$    & $m \land c$   & $\neg(m \land c)$ & $(m \lor c) \land \neg(m \land c)$ \\ \hline
                T   & T     & T             & T             & F                 & F
                \\
                T   & F     & T             & F             & T                 & T
                \\
                F   & T     & T             & F             & T                 & T
                \\
                F   & F     & F             & F             & T                 & F
            \end{tabular}

            ~\\
            Example 2: $(m \land \neg c) \lor (\neg m \land c)$

            ~\\
            \begin{tabular}{c | c || c || c || c || c || c}
                $m$ & $c$   & $\neg m$  & $\neg c$  & $(m \land \neg c)$    & $(\neg m \land c)$    & $(m \land \neg c) \lor (\neg m \land c)$ \\ \hline
                T   & T     & F         & F         & F                     & F                     & F
                \\
                T   & F     & F         & T         & T                     & F                     & T
                \\
                F   & T     & T         & F         & F                     & T                     & T
                \\
                F   & F     & T         & T         & F                     & F                     & F
            \end{tabular}
        }{}

    \end{questionNOGRADE}

    \notonkey{
    ~\\
    \small
    When you look at the column for your final expression, here's how you verify your logic is correct:
    The result should be \textbf{true} when (1) $m$ is true and $c$ is false, OR (2) $m$ is false and $c$ is true.
    (one or the other is in the fridge, but not both.)
    The result should be \textbf{false} for (1) $m$ and $c$ are both true (both in the fridge), OR
    (2) $m$ and $c$ are both false (neither are in the fridge).
    \normalsize

    \newpage

    \section*{Overview questions}
    }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		What is the result of each of the following statements?
		\begin{itemize}
			\item[a.]   $p \land \neg q$ when $p$ is true and $q$ is true.      \solution{ ~\\ true $\land$ not-true \\ = true $\land$ false \\ = FALSE}{ \vspace{1cm} }
			\item[b.]   $p \lor \neg q$ when $p$ is false and $q$ is false.     \solution{ ~\\ false $\lor$ not-false \\ = false $\lor$ true \\ = TRUE }{ \vspace{1cm} }
			\item[c.]   $\neg( p \lor q )$ when $p$ is false and $q$ is false.  \solution{ ~\\ not-(false $\lor$ false) \\ = not-false \\ = TRUE }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		Translate the following statements into propositional logic using the given variables:
		\begin{center}
			$b$: The sandwich has bacon \tab $l$: The sandwich has lettuce \\
			$t$: The sandwich has tomato \tab $c$: The sandwich has cheese
		\end{center}
		\begin{itemize}
			\item[a.]   The sandwich has bacon, lettuce, and tomato.
                        \solution{ ~\\ $b \land l \land t$ }{ \vspace{1cm} }
			\item[b.]   The sandwich has tomato and it doesn't have cheese.
                        \solution{ ~\\ $t \land \neg c$ }{ \vspace{1cm} }
			\item[c.]   The sandwich has tomato, but it doesn't have neither bacon nor cheese.
                        \solution{ ~\\ $t \land \neg b \land \neg c$ }{ \vspace{1cm} }
			\item[d.]   Either the sandwich has (only) lettuce and tomato, or it has (only) bacon and cheese.
                        \solution{ ~\\
                            $(l \land t \land \neg b \land \neg c) \lor (\neg l \land \neg t \land b \land c)$
                        }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		Using the propositional variables, $p$, $q$, and $r$, write compound statements that meets each of the given criteria.
		\begin{itemize}
			\item[a.]   $p$ and $q$ are true, but not $r$.
                        \solution{ ~\\ $p \land q \land \neg r$ }{ \vspace{1cm} }
			\item[b.]   $p$ and one other statement are true, but not all three.
                        \solution{ ~\\
                            Solution 1: $p \land (q \lor r) \land \neg (q \land r)$ \\
                            Solution 2: $(p \land q \land \neg r) \lor (p \land \neg q \land r)$
                         }{ \vspace{1cm} }
			\item[c.]   Exactly two statements are true, but not only one and not all three.
                        \solution{ ~\\
                        $(p \land q \land \neg r) \lor (p \land \neg q \land r) \lor (\neg p \land q \land r)$
                         }{ \vspace{1cm} }
		\end{itemize}
    \end{questionNOGRADE}

    \solution{ \newpage }{}

    \stepcounter{question}
    \begin{questionNOGRADE}{\thequestion}
		Complete the truth tables for the following statements:
		\begin{itemize}
			\item[a.]   $p \land \neg q$
                        \solution{
                            ~\\
                            \begin{tabular}{c | c || c}
                                $p$ & $q$ & $p \land \neg q$ \\ \hline
                                T & T & F
                                \\
                                T & F & T
                                \\
                                F & T & F
                                \\
                                F & F & F
                            \end{tabular}
                        }{ \vspace{3cm} }

			\item[b.]   $\neg p \lor q$
                        \solution{
                            ~\\
                            \begin{tabular}{c | c || c}
                                $p$ & $q$ & $\neg p \lor q$ \\ \hline
                                T & T & T
                                \\
                                T & F & F
                                \\
                                F & T & T
                                \\
                                F & F & T
                            \end{tabular}
                        }{ \vspace{3cm} }

			\item[c.]   $(p \land q) \lor (p \land r)$
                        \solution{
                            ~\\
                            \begin{tabular}{c | c | c || c}
                                $p$ & $q$ & r & $(p \land q) \lor (p \land r)$ \\ \hline
                                T & T & T & T
                                \\
                                T & T & F & T
                                \\
                                T & F & T & T
                                \\
                                T & F & F & F
                                \\
                                F & T & T & F
                                \\
                                F & T & F & F
                                \\
                                F & F & T & F
                                \\
                                F & F & F & F
                            \end{tabular}
                        }{ \vspace{3cm} }

			\item[d.]   $p \land \neg (q \lor r)$
                        \solution{
                            ~\\
                            \begin{tabular}{c | c | c || c}
                                $p$ & $q$ & r & $p \land \neg (q \lor r)$ \\ \hline
                                T & T & T & F
                                \\
                                T & T & F & F
                                \\
                                T & F & T & F
                                \\
                                T & F & F & T
                                \\
                                F & T & T & F
                                \\
                                F & T & F & F
                                \\
                                F & F & T & F
                                \\
                                F & F & F & F
                            \end{tabular}
                            \newpage
                        }{ \vspace{3cm} }

			\item[e.]   $(p \lor q) \lor (\neg p \lor \neg q)$
                        \solution{
                            ~\\
                            \begin{tabular}{c | c || c}
                                $p$ & $q$ & $(p \lor q) \lor (\neg p \lor \neg q)$ \\ \hline
                                T & T & T
                                \\
                                T & F & T
                                \\
                                F & T & T
                                \\
                                F & F & T
                            \end{tabular}
                        }{ \vspace{3cm} }

			\item[f.]   $p \land \neg p$
                        \solution{
                            ~\\
                            \begin{tabular}{c || c }
                                $p$ & $p \land \neg p$ \\ \hline
                                T & F 
                                \\
                                F & T 
                            \end{tabular}
                        }{ \vspace{3cm} }
		\end{itemize}
    \end{questionNOGRADE}



\input{../BASE-4-FOOT}
