
    
    \begin{answer}
    
    
    
    \section*{Predicates and Quantifiers - Answer key}

    \begin{enumerate}
        \item
            \begin{itemize}
                \item[a.]   $P(1) = 3 \cdot 1 = 3$; $P(1)$ is false.
                \item[b.]   $P(2) = 3 \cdot 2 = 6$; $P(2)$ is true.
                \item[c.]   $P(5) = 3 \cdot 5 = 15$; $P(5)$ is false.
                \item[d.]   $P(10) = 3 \cdot 10 = 30$; $P(10)$ is true.
                \item[e.]   $P(2x) = 3 \cdot 2x = 2(3x)$; $P(2x)$ is true. (2 times any integer is always even.)
            \end{itemize}
        \item
            \begin{itemize}
                \item[a.]   True for some.
                \item[b.]   True for some - the set of natural numbers includes 0, which is not positive.
                \item[c.]   True for all - we will go into proofs on this later in this class.
                \item[d.]   True for none - $2n+1$ will always result in an odd number. We will learn the proof later.
            \end{itemize}
        \item
            \begin{itemize}
                \item[a.]   $T(x)$ is the predicate, ``$x$ is tired''. $C$ is the set of all college students. (You might have used different letters in your definitions.) ~\\ Statement: $\forall x \in C, T(x)$
                \item[b.]   $P(x)$ is the predicate, ``$x$ does not like pizza''. $C$ is the set of all college students. $\exists x \in C, P(x)$ ~\\ (You might have also defined, $P(x)$ is ``$x$ likes pizza'', which would give you $\exists x \in C, \neg P(x)$)
                \item[c.]   $S(x)$ is the predicate, ``$x$ studies''. $P(x)$ is the predicate, ``$x$ passes their class''. $C$ is the set of all college students. ~\\ $\forall x \in C, S(x) \to P(x)$
                \item[d.]   $S(x)$ is the predicate, ``$x$ studies''. $P(x)$ is the predicate, ``$x$ passes their class''. $C$ is the set of all college students. ~\\ $\exists x \in C, S(x) \land \neg P(x)$
            \end{itemize}
        \item
            \begin{itemize}
                \item[a.]   Many answers. Example: Domain is $\{ 101, 102, 103 \}$
                \item[b.]   Many answers. Example: Domain is $\{ -1, -2, -3 \}$
                \item[c.]   Many answers. Example: Domain is $\{ 4, 9, 16, 25 \}$
            \end{itemize}
        \newpage
        \item
            \begin{itemize}
                \item[a.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in D, E(x) \land P(x)$, $D = \{ 2, 4, 6, 8 \}$
                                \\          & ``For all elements $x$ in $D$, $x$ is even and $x$ is positive.''
                                \\
                                Negation:   & $\neg ( \forall x \in D, E(x) \land P(x) )$
                                \\          & $\neg ( \forall x \in D ), \neg( E(x) \land P(x) )$
                                \\          & $\exists x \in D, \neg E(x) \lor \neg P(x)$
                                \\
                                English:    & There exists some $x$ in $D$ such that $x$ is not even or $x \leq 0$.
                                \\
                                Result:     & The original is true. The negation is false: No numbers are negative NOR are they odd.
                            \end{tabular} 
                \item[b.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in E, O(x) \land P(x)$, $E = \{ 1, 2, 3, 4 \}$
                                \\          & ``For all elements $x$ in $D$, $x$ is odd and $x$ is positive.''
                                \\
                                Negation:   & $\neg ( \forall x \in E, O(x) \land P(x) )$
                                \\          & $\neg ( \forall x \in E ), \neg( O(x) \land P(x) )$
                                \\          & $\exists x \in D, \neg O(x) \lor \neg P(x)$
                                \\
                                English:    & There exists some $x$ in $D$ such that $x$ is not odd or $x \leq 0$.
                                \\
                                Result:     & The original is false: Not all numbers are odd. The negation is true.
                            \end{tabular}
                \item[c.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in \mathbb{Z}, N(x) \to \neg P(x)$
                                \\          & ``For all integers $x$, if $x$ is negative then $x$ is not positive.''
                                \\
                                Negation:   & $\neg ( \forall x \in \mathbb{Z}, N(x) \to \neg P(x) )$
                                \\          & $\neg ( \forall x \in \mathbb{Z} ), \neg( N(x) \to \neg P(x) )$
                                \\          & $\exists x \in \mathbb{Z}, N(x) \land \neg ( \neg P(x) )$
                                \\          & $\exists x \in \mathbb{Z}, N(x) \land P(x)$
                                \\
                                English:    & There exists some integer $x$ such that $x < 0$ and $x > 0$.
                                \\
                                Result:     & The original is true. The negation is false: A number cannot be less than 0 and greater than 0 at the same time.
                            \end{tabular}
            \end{itemize}
    \end{enumerate}
    \end{answer}

    \chapter*{1.6: Predicates and Nested Quantifiers}
    
    \begin{enumerate}
        \item   Negate each of the following statements:
            \begin{itemize}
                \item[a.]   $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x)$
                \item[b.]   $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \land Q(x)$
                \item[c.]   $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \lor Q(x)$
                \item[d.]   $\exists x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \to Q(x)$
                \item[e.]   $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \lor Q(x) \to R(x)$
                \item[f.]   $\forall x \in \mathbb{Z}, \forall y \in \mathbb{Z}, P(x) \to Q(x) \land R(x)$
            \end{itemize}
    \end{enumerate}
        
    \newpage
    \begin{answer}
    \section*{Predicates and Nested Quantifiers - Answer key}
    
    \begin{enumerate}
        \item   Negate each of the following statements:
            \begin{itemize}
                \item[a.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x)$
                                \\
                                Negation:   & $\neg ( \forall x \in \mathbb{Z},     \exists y \in \mathbb{Z}, P(x) ) $
                                \\          & $\neg ( \forall x \in \mathbb{Z} ),   \neg ( \exists y \in \mathbb{Z} ), \neg ( P(x) ) $
                                \\          & $\exists x \in \mathbb{Z},            \forall y \in \mathbb{Z}, \neg P(x) $
                            \end{tabular}
                            
                \item[b.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \land Q(x)$
                                \\
                                Negation:   & $\neg ( \forall x \in \mathbb{Z},     \exists y \in \mathbb{Z}, P(x) \land Q(x) ) $
                                \\          & $\neg ( \forall x \in \mathbb{Z} ),   \neg ( \exists y \in \mathbb{Z} ), \neg ( P(x) \land Q(x) ) $
                                \\          & $\exists x \in \mathbb{Z},            \forall y \in \mathbb{Z}, \neg P(x) \lor \neg Q(x) $
                            \end{tabular}
                            
                \item[c.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \lor Q(x)$
                                \\
                                Negation:   & $\neg ( \forall x \in \mathbb{Z},     \exists y \in \mathbb{Z}, P(x) \lor Q(x) ) $
                                \\          & $\neg ( \forall x \in \mathbb{Z} ),   \neg ( \exists y \in \mathbb{Z} ), \neg ( P(x) \lor Q(x) ) $
                                \\          & $\exists x \in \mathbb{Z},            \forall y \in \mathbb{Z}, \neg P(x) \land \neg Q(x) $
                            \end{tabular}
                            
                \item[d.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\exists x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \to Q(x)$
                                \\
                                Negation:   & $\neg ( \exists x \in \mathbb{Z},     \exists y \in \mathbb{Z}, P(x) \to Q(x) ) $
                                \\          & $\neg ( \exists x \in \mathbb{Z} ),   \neg ( \exists y \in \mathbb{Z} ), \neg ( P(x) \to Q(x) ) $
                                \\          & $\forall x \in \mathbb{Z},            \forall y \in \mathbb{Z}, P(x) \land \neg Q(x)  $
                            \end{tabular}
                            
                \item[e.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in \mathbb{Z}, \exists y \in \mathbb{Z}, P(x) \lor Q(x) \to R(x)$
                                \\
                                Negation:   & $\neg ( \forall x \in \mathbb{Z},     \exists y \in \mathbb{Z}, P(x) \lor Q(x) \to R(x) ) $
                                \\          & $\neg ( \forall x \in \mathbb{Z} ),   \neg ( \exists y \in \mathbb{Z} ), \neg (  P(x) \lor Q(x) \to R(x) ) $
                                \\          & $\exists x \in \mathbb{Z},            \forall y \in \mathbb{Z}, P(x) \lor Q(x) \land \neg R(x) $
                            \end{tabular}
                            
                \item[f.]   ~\\
                            \begin{tabular}{l p{9cm}}
                                Original:   & $\forall x \in \mathbb{Z}, \forall y \in \mathbb{Z}, P(x) \to Q(x) \land R(x)$
                                \\
                                Negation:   & $\neg ( \forall x \in \mathbb{Z},     \forall y \in \mathbb{Z}, P(x) \to Q(x) \land R(x) ) $
                                \\          & $\neg ( \forall x \in \mathbb{Z} ),   \neg ( \forall y \in \mathbb{Z} ), \neg ( P(x) \to Q(x) \land R(x) ) $
                                \\          & $\exists x \in \mathbb{Z}, \exists y  \in \mathbb{Z}, P(x) \land \neg( Q(x) \land R(x) )  $
                                \\          & $\exists x \in \mathbb{Z}, \exists y  \in \mathbb{Z}, P(x) \land ( \neg Q(x) \lor \neg R(x) )  $
                            \end{tabular}
            \end{itemize}
    \end{enumerate}
    \end{answer}
