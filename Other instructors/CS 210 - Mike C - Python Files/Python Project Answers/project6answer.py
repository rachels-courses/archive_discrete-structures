# project6answer.py

def typeOfDigit(a,b):
    if (a * b) % 2 == 0:
        return 'EVEN'
    else:
        return 'ODD'
    
def main():
    print('even * even =', typeOfDigit(4,6), 'number.')
    print('even * odd =', typeOfDigit(4,3), 'number.')
    print('odd * odd =', typeOfDigit(3,7), 'number.')
    
main()
