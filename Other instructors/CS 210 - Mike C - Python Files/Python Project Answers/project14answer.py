# josephusForAny.py

# this program will find the last person standing with any skip number



def populateList(count):
    listOfPeople = []
    for i in range(1, count+1):
        listOfPeople.append(i)
    return listOfPeople

def lastOneStanding(count,skip):
    currIndex = 0
    counter = 1  #keeps track of how many indices I've traveled over, starts on first index
    people = populateList(count)
    while people.count(None) < len(people) - 1:
        if counter == skip:
            if people[currIndex % count] == None:
                currIndex += 1
            else:
                people[currIndex % count] = None
                counter = 1
        else:
            if people[currIndex % count] == None:
                currIndex += 1
            else:
                currIndex += 1
                counter += 1
      
    for i in people:
        if i != None:
            return i


def main():
    count = int(input("Enter how many are in the circle: "))
    skip = int(input("Enter the skip number: "))
    print("Stand in position", lastOneStanding(count, skip))

main()
