# project7answer.py

def decToBinary(d):
    quotient = d
    answer = ""
    while quotient > 0:
        quotient, remainder = divmod(quotient,2)
        answer += str(remainder)
    return answer[::-1]  # reverses the string variable answer

def simple(d):
    return bin(d)

def main():
    d = int(input("Enter an integer greater than 0: "))
    print(decToBinary(d))
    print(simple(d))

main()
        
