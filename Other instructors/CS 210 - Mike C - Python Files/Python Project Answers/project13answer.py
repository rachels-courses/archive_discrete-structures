# project13answer.py

#Write a program that prompts the user to enter a positive integer n and displays its prime 
#	factors separated by *. For example:
#            Enter a positive integer: 90 
#               90 = 2 * 3 * 3 * 5


def findFactors(p):
    n = 2
    result= str(p) + " = "
    while n <= p:
        if p % n == 0:
            result += str(n) + " * "
            p = p/n
        else:
            n = n+1
    
    return result[:-2]

def main():
    p = int(input("Enter an integer greater than 1: "))
    print(findFactors(p))
    

main()
            
