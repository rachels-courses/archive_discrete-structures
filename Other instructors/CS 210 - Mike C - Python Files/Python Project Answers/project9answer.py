#encryptdecrypt.py

# we must first determine our secret key
# step 1: pick two prime numbers and use the product for n (public value)
#         so each number to be encrypted is between 0 and n-1
#         I pick 17 * 3 = 51 = n
# step 2: compute k = (p - 1) * (q - 1)
#         Mine is k = 16 * 2 = 32
# step 3: we need another prime number between 1 and k, that is NOT a factor of k
#         this number is called e and is public also
#         Based on 32, k cannot have a factor of 2
#         So I pick e = 3 (small)
# step 4: use k and e to find positive integers d and v so that the equation:
#           d*e - v*k = 1  is true
#         For my e = 3 and k = 32, my equation is: 3d - 32v = 1
#         So d = 11, v = 1
# step 5: only the number d, e, and n are kept
#         d = 11, e = 3, n = 51
#
# IMPORTANT!
#  encrypt a number smaller than the value of n or you will not receive the original
#   ciphertext. why not?
#  because you will no longer have a unique value returned, so your original value
#   will be lost.

def encrypt(m, e, n):
    return (m ** e) % n

def decrypt(m, d, n):
    return (m ** d) % n

def main():
    print(encrypt(39,3,51))
    print(decrypt(6,11,51))

main()
