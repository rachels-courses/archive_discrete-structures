# project5answer.py

def implication(p,q):
    if p == True and q == False:
        return False
    else:
        return True


def main():
    print("Should be false (t,f):", implication(1,0)) # can use 1/0 as True/False
    print("Should be true (t,t):", implication(1,1))
    print("Should be true (f,f):", implication(0,0))
    print("Should be true (f,t):", implication(0,1))

    print("Should be false (t,f):", implication(True,False)) # could use True/False also 
    print("Should be true (t,t):", implication(True,True))
    print("Should be true (f,f):", implication(False,False))
    print("Should be true (f,t):", implication(False,True))

   
main()
