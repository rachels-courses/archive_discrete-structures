# project2answer.py


x = 3
y = 6
z = 'abc'

print(x + y)
print(x * y)
print(x ** y)
print(x / y)
print(x // y)
print(y / x)
print(y // x)
#print(y + z)
#python does not an implicit conversion to a string during concatenation
#  in this case, y must be explicitly converted to a string
print(str(y) + z)
