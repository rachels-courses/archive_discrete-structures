# Whit Sargent CS210
total = 0
answer = 'Y'
while answer == 'Y' or answer == 'y':
    num = input('What is the number you want to sum up to? ')
    for x in range(1, int(num)+1):
        total += x
    print(total)
    answer = input('Would you like to try another number? (Y/N) ')
    total = 0
