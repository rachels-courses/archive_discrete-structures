#CS210proj3AndrewTribble
#import sys
import sys
#define main method
def main():
    try:
        #Ask user input
        n = int(input("Please enter a value for n: "))
        sys.stdout.write( "\n" )
        #computer output
        nSum = 0
        nSum = ((n * (n + 1)))
        #print output
        print("The sum of the numbers between 1 and", n,"is", (int)(nSum/2))
        sys.stdout.write("\n\n\n\n")
        #start program over
        main();
    except:
        sys.stdout.write( "\n" )
        print("Error!")
        sys.stdout.write("\n\n\n\n")
        main()

#initiate program
main();
