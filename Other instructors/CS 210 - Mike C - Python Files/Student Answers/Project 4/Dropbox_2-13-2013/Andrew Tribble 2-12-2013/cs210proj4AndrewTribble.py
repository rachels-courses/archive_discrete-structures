#cs210proj4AndrewTribble

def main():
    a = input('Please state a boolean value for a:')
    b = input('Please state a boolean value for b:')

    if a == 'True':
        c = True
    elif a == 'true':
        c = True
    elif a == 'T':
        c = True
    elif a == 't':
        c = True
    elif a == 'False':
        c = False
    elif a == 'false':
        c = False
    elif a == 'F':
        c = False
    elif a == 'f':
        c = False

    if b == 'True':
        d = True
    elif b == 'true':
        d = True
    elif b == 'T':
        d = True
    elif b == 't':
        d = True
    elif b == 'False':
        d = False
    elif b == 'false':
        d = False
    elif b == 'F':
        d = False
    elif b == 'f':
        d = False
       
    e = c and d
    f = c or d
    g = not c
    h = not d

    print(e)
    print(f)
    print(g)
    print(h)
    print()
    print()
    print()

    main()

main()
