One = input("Input either True or False: ")
Two = input("Input either True or False again: ")
#For some reason even putting in True or False treats these as strings.
#How annoying. :(
#Check to see if any are true. Treat invalids as False.
One = One in ["True", "true", "T", "t"]
Two = Two in ["True", "true", "T", "t"]
#Confirm inputs...
print("One: ", One)
print("Two: ", Two)
#Test these then...
print("One AND Two: ", One and Two)
print("One OR Two: ", One or Two)
print("NOT One: ", not One)
