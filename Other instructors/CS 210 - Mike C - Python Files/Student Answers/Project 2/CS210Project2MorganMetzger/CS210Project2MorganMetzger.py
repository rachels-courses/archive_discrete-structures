#CS210Project2MorganMetzger.py

x = 3
y = 6
z = 'abc'

print(x+y)
print(x*y)
print(x**y)
print(x/y)
print(x//y)
print(y/x)
print(y//x)
#print(y+z) this line will not concatenate because it has two different types int and string.
#So I type cast the int value into a string.
print(str(y)+z)



