x = 3
y = 6
z = 'abc'

print(x+y)
print(x*y)
print(x**y)
print(x/y)
print(x//y)
print(y/x)
print(y//x)
#print(y+z)
"""
    
    The line above will throw an error, the reason being
    you cannot perform arithmetic opperands between strings and ints.
    You must print the two variables differently, as seen below.
    
    error thrown: "TypeError: unsupported operand type(s) for +: 'int' and 'str'"
    
"""
print(y, z)
