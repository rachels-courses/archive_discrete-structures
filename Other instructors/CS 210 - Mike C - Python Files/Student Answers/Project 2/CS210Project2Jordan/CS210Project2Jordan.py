x = 3
y = 6
z = 'abc'
print(x + y)    
print(x * y) 
print(x**y)
print(x/y)
print(x//y)
print(y/x)
print(y//x)
#print(y+z) Produces an error since y is not a string, its a variable and you can't add variable with string
print(str(y)+z)
