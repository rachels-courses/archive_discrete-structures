# implication.py

def implication(p,q):
    if p == True and q == False:
        return False
    else:
        return True

def main():
    print("Should be false:", implication(1,0))
    print("Should be true:", implication(1,1))
    print("Should be true:", implication(0,0))
    print("Should be true:", implication(0,1))

main()
