# finddigits.py

from math import log10, ceil

# this function takes a while for a large number since...
# the multiplication takes place and then the len function is evaluated
def findDigits(n):
    return len(str(eval(n)))

def findDigitsLog(n):
    b,x = n.split('**')
    return ceil(float(x) * log10(float(b)))
        
def main():
    n = input("Enter an integer in the form, b ** x, where b = base, x = exponent: ")
    print(findDigits(n))
    print("log answer: ", findDigitsLog(n))
    

main()    
