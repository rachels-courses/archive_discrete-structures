#decimaltooctal.py

def decToOctal(d):
    quotient = d
    answer = ""
    while quotient > 0:
        quotient, remainder = divmod(quotient,8)
        answer += str(remainder)
    return answer[::-1]  # reverses the string variable answer

def main():
    d = int(input("Enter an integer greater than 0: "))
    print(decToOctal(d))

main()
        
