# sum1CubedToNCubed

# square the result of sum1ToN function
def sum1CubedToNCubed(n):
    return int((n * (n + 1) / 2)**2)

def main():
    n = int(input("Enter a positive integer: "))
    print("Sum =" , sum1CubedToNCubed(n))

main()
