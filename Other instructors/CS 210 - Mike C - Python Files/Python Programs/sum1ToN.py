# sum1ToN

def sum1ToN(n):
    return int(n * (n + 1) / 2)

def main():
    n = int(input("Enter a positive integer: "))
    print("Sum =" , sum1ToN(n))

main()
