#binarytodecimal.py

def binToDecimal(b):
    decimal = 0
    lengthOfBinary = len(b)
    for digit in b:
        lengthOfBinary -= 1
        decimal += int(digit) * (2 ** lengthOfBinary)
    
    return decimal

def binToDecimal2(b):  # using the int function
    return int(b, 2) 

def main():
    b = input("Enter an binary number of at least 0: ")
    print(binToDecimal(b))
    print(binToDecimal2(b))

main()
        
