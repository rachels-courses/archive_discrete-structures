# Extra Credit - Chapter 7 topics

## Mini-Essay

Look up [applications of Graph Theory](https://en.wikipedia.org/wiki/Graph_theory#Applications)
and write a paragraph or two about an application that interests you,
and how graphs are applied to it.

## Turn in

Turn in a Word, LibreOffice, or text document in Canvas.
